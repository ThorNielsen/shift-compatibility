cmake_minimum_required(VERSION 2.8.7)

include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/Macros.cmake)

use_cpp17()
add_compile_options("-Wall" "-Wextra" "$<$<CONFIG:Release>:-O3;-s>")

# Quick hack: Enable debugging symbol generation if not a release build (as
# opposed to enabling only on debug builds).
add_compile_options("$<$<NOT:$<CONFIG:Release>>:-O0;-g>")

project("Shift compatibility")

include_directories(${PROJECT_SOURCE_DIR}/include)
add_subdirectory(src)

