#ifndef FORMATTING_HPP_INCLUDED
#define FORMATTING_HPP_INCLUDED

#include "types.hpp"

// Set this to true to enable coloured terminal output (recommended).
// If your terminal doesn't support colour codes, or you want to save the output
// to a file, set this to false instead.
extern std::atomic<bool> g_useColourCodes;

// The rest of this file is just definitions of formatting operators.
// Nothing to see here -- move along.
template <U32 EdgeCount>
std::ostream& operator<<(std::ostream& ost, const Path<EdgeCount>& path)
{
    const char* vBegin = g_useColourCodes ? "\033[91m" : "v(";
    const char* eBegin = g_useColourCodes ? "\033[92m" : "e(";
    const char* end = g_useColourCodes ? "\033[0m " : ")-";
    const char* terminal = g_useColourCodes ? "\033[0m" : ")";

    for (U32 i = 0; i < EdgeCount; ++i)
    {
        ost << vBegin << path.vertices[i] << end
            << eBegin << path.edges[i] << end;
    }
    ost << vBegin << path.vertices[EdgeCount] << terminal;
    return ost;
}

std::ostream& operator<<(std::ostream& ost, const DualIsomorphismEntry& entry)
{
    const char* vBegin = g_useColourCodes ? "\033[91m" : "v(";
    const char* eBegin = g_useColourCodes ? "\033[92m" : "e(";
    const char* end = g_useColourCodes ? "\033[0m " : ")-";
    const char* terminal = g_useColourCodes ? "\033[0m" : ")";

    for (U32 i = 0; i < entry.permutation.size(); ++i)
    {
        const auto& src = entry.sourcePath[i];
        const auto& dst = entry.destPath[entry.permutation[i]];
        ost << vBegin << entry.startVertex << end
            << eBegin << src.e1 << end
            << vBegin << src.middleVertex << end
            << eBegin << src.e2 << end
            << vBegin << entry.endVertex << terminal;

        ost << " --> ";

        ost << vBegin << entry.startVertex << end
            << eBegin << dst.e1 << end
            << vBegin << dst.middleVertex << end
            << eBegin << dst.e2 << end
            << vBegin << entry.endVertex << terminal;
        if (i + 1 < entry.permutation.size()) ost << "\n";
    }
    return ost;
}

std::ostream& operator<<(std::ostream& ost, const DualPathIsomorphism& iso)
{
    U32 height = iso.entries.size() / iso.width;
    for (U32 i = 0; i < iso.width; ++i)
    {
        for (U32 j = 0; j < height; ++j)
        {
            ost << iso(i, j);
            if (i + 1 < iso.width || j + 1 < height) ost << "\n\n";
        }
    }
    return ost;
}

std::ostream& operator<<(std::ostream& ost, Permutation perm)
{
    for (U32 i = 0; i < perm.size(); ++i)
    {
        if (perm[i] < perm.size()) ost << perm[i];
        else ost << '-';
        if (i+1 < perm.size()) ost << ' ';
    }
    return ost;
}

#endif // FORMATTING_HPP_INCLUDED
