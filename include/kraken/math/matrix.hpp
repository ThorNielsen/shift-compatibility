// This file is part of the Kraken project, and in addition to the license for
// that project (if any), this file itself is also available under the MIT
// License, the text of which follows below.

// MIT License

// Copyright (c) 2022 Thor Gabelgaard Nielsen

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef KRAKEN_MATH_MATRIX_HPP_INCLUDED
#define KRAKEN_MATH_MATRIX_HPP_INCLUDED

#include <kraken/types.hpp>
#include <kraken/math/comparison.hpp>
#include <kraken/math/quaternion.hpp>
#include <cmath>
#include <cstddef>
#include <initializer_list>
#include <ostream>
#include <stdexcept>
#include <type_traits>

namespace kraken
{

namespace math
{

template <size_t Rows, size_t Cols, typename Prec>
class Matrix
{
public:
    explicit Matrix(Prec diag);
    Matrix() = default;
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() == 1)
        {
            for (size_t i = 0; i < Rows; ++i)
            {
                for (size_t j = 0; j < Cols; ++j)
                {
                    (*this)(i, j) = (i == j) * *list.begin();
                }
            }
            return;
        }
        if (list.size() != Rows * Cols)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        auto* writeAt = m_data;
        while (beg != list.end())
        {
            *writeAt++ = *beg++;
        }
    }
    template <typename = typename std::enable_if<(Rows >1)>>
    Matrix(std::initializer_list<Matrix<Rows, 1, Prec>> list)
    {
        if (list.size() != Cols)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        for (size_t i = 0; i < Cols; ++i)
        {
            setCol(i, *beg++);
        }
    }
    //template <typename = typename std::enable_if<(Rows >1)>>
    Matrix(std::initializer_list<Matrix<1, Cols, Prec>> list)
    {
        if (list.size() != Rows)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        for (size_t i = 0; i < Rows; ++i)
        {
            setRow(i, *beg++);
        }
    }
    template <typename OPrec>
    Matrix(const Matrix<Rows, Cols, OPrec>& other)
    {
        for (size_t i = 0; i < Rows*Cols; ++i)
        {
            m_data[i] = static_cast<Prec>(other.data()[i]);
        }
    }

    template <size_t ORows, size_t OCols, typename OPrec,
              typename = typename std::enable_if<(ORows <= Rows && OCols <= Cols
                                                 && (ORows < Rows || OCols < Cols))>>
    Matrix(const Matrix<ORows, OCols, OPrec>& other, Prec diag = Prec(1))
    {
        for (size_t i = 0; i < Rows; ++i)
        {
            for (size_t j = 0; j < Cols; ++j)
            {
                if (i < ORows && j < OCols)
                {
                    (*this)(i, j) = other(i, j);
                }
                else
                {
                    (*this)(i, j) = (i == j)*diag;
                }
            }
        }
    }

    template <typename = typename std::enable_if<(Rows >= 3 && Cols >= 3)>>
    Matrix(const quat_t<Prec>& q)
        : Matrix(Prec(1))
    {
        (*this)(0, 0) = Prec(1) - Prec(2) * (q.y * q.y + q.z * q.z);
        (*this)(0, 1) = Prec(2)           * (q.x * q.y - q.w * q.z);
        (*this)(0, 2) = Prec(2)           * (q.x * q.z + q.w * q.y);
        (*this)(1, 0) = Prec(2)           * (q.x * q.y + q.w * q.z);
        (*this)(1, 1) = Prec(1) - Prec(2) * (q.x * q.x + q.z * q.z);
        (*this)(1, 2) = Prec(2)           * (q.y * q.z - q.w * q.x);
        (*this)(2, 0) = Prec(2)           * (q.x * q.z - q.w * q.y);
        (*this)(2, 1) = Prec(2)           * (q.y * q.z + q.w * q.x);
        (*this)(2, 2) = Prec(1) - Prec(2) * (q.x * q.x + q.y * q.y);
    }

    Prec* data() { return m_data; }
    const Prec* data() const { return m_data; }

    size_t rowCount() const { return Rows; }
    size_t colCount() const { return Cols; }

     template <class Return = Prec>
     typename std::enable_if<Cols == 1 || Rows == 1, Return>::type
     operator[](size_t i) const
     {
         return m_data[i];
     }

     template <class Return = Prec>
     typename std::enable_if<Cols == 1 || Rows == 1, Return&>::type
     operator[](size_t i)
     {
         return m_data[i];
     }

    template <class Return = Prec>
    typename std::enable_if<Cols == 1 || Rows == 1, Return>::type
    operator()(size_t i) const
    {
        return m_data[i];
    }

    template <class Return = Prec>
    typename std::enable_if<Cols == 1 || Rows == 1, Return&>::type
    operator()(size_t i)
    {
        return m_data[i];
    }

    Prec operator()(size_t i, size_t j) const
    {
        return m_data[toIndex(i, j)];
    }

    Prec& operator()(size_t i, size_t j)
    {
        return m_data[toIndex(i, j)];
    }

    Matrix<1, Cols, Prec> row(size_t i) const
    {
        Matrix<1, Cols, Prec> r;
        for (size_t j = 0; j < Cols; ++j)
        {
            r(j) = (*this)(i, j);
        }
        return r;
    }

    void setRow(size_t i, const Matrix<1, Cols, Prec>& r)
    {
        for (size_t j = 0; j < Cols; ++j)
        {
            (*this)(i, j) = r(j);
        }
    }
    template <size_t Length = Cols>
    typename std::enable_if<Length != 1, void>::type
    setRow(size_t i, const Matrix<Cols, 1, Prec>& r)
    {
        for (size_t j = 0; j < Cols; ++j)
        {
            (*this)(i, j) = r(j);
        }
    }

    Matrix<Rows, 1, Prec> col(size_t j) const
    {
        Matrix<Rows, 1, Prec> c;
        for (size_t i = 0; i < Rows; ++i)
        {
            c(i) = (*this)(i, j);
        }
        return c;
    }

    void setCol(size_t j, const Matrix<Rows, 1, Prec>& c)
    {
        for (size_t i = 0; i < Rows; ++i)
        {
            (*this)(i, j) = c(i);
        }
    }
    template <size_t Length = Rows>
    typename std::enable_if<Length != 1, void>::type
    setCol(size_t j, const Matrix<1, Rows, Prec>& c)
    {
        for (size_t i = 0; i < Rows; ++i)
        {
            (*this)(i, j) = c(i);
        }
    }

    Matrix<Rows, Cols, Prec>& operator+=(const Matrix<Rows, Cols, Prec>& other);
    Matrix<Rows, Cols, Prec>& operator-=(const Matrix<Rows, Cols, Prec>& other);
    Matrix<Rows, Cols, Prec>& operator*=(const Matrix<Cols, Cols, Prec>& other);

    template <typename OPrec>
    Matrix<Rows, Cols, Prec>& operator*=(OPrec scalar)
    {
        for (size_t i = 0; i < Rows * Cols; ++i)
        {
            m_data[i] *= scalar;
        }
        return *this;
    }
    template <typename OPrec>
    Matrix<Rows, Cols, Prec>& operator/=(OPrec scalar)
    {
        for (size_t i = 0; i < Rows * Cols; ++i)
        {
            m_data[i] /= scalar;
        }
        return *this;
    }

    Matrix<Rows, Cols, Prec> operator+() const
    {
        Matrix<Rows, Cols, Prec> m;
        for (size_t i = 0; i < Rows*Cols; ++i)
        {
            m.m_data[i] = +m_data[i];
        }
        return m;
    }

    Matrix<Rows, Cols, Prec> operator-() const
    {
        Matrix<Rows, Cols, Prec> m;
        for (size_t i = 0; i < Rows*Cols; ++i)
        {
            m.m_data[i] = -m_data[i];
        }
        return m;
    }

    Matrix<Cols, Rows, Prec> transpose() const
    {
        Matrix<Cols, Rows, Prec> mat;
        for (size_t i = 0; i < Rows; ++i)
        {
            for (size_t j = 0; j < Cols; ++j)
            {
                mat(j, i) = (*this)(i, j);
            }
        }
        return mat;
    }

private:
    size_t toIndex(size_t i, size_t j) const
    {
        if (i*Cols + j > Rows * Cols) throw std::domain_error("Out of bounds read.");
        return i * Cols + j;
    }
    Prec m_data[Rows*Cols];
};

////////////////////////////////////////////////////////////////////////////////
/// Basic operations (implementations).
////////////////////////////////////////////////////////////////////////////////

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>::Matrix(Prec diag)
{
    for (size_t i = 0; i < Rows; ++i)
    {
        for (size_t j = 0; j < Cols; ++j)
        {
            m_data[toIndex(i, j)] = i == j ? diag : Prec(0);
        }
    }
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec> operator+(Matrix<Rows, Cols, Prec> first,
                                          const Matrix<Rows, Cols, Prec>& second)
{
    return first += second;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec> operator-(Matrix<Rows, Cols, Prec> first,
                                          const Matrix<Rows, Cols, Prec>& second)
{
    return first -= second;
}

#ifdef KRAKEN_ALLOW_DEGENERATE_MATRICES
template <size_t RowsA, size_t SharedSize, size_t ColsB, typename Prec>
inline Matrix<RowsA, ColsB, Prec>
operator*(Matrix<RowsA, SharedSize, Prec> first,
         const Matrix<SharedSize, ColsB, Prec>& second)
{
   Matrix<RowsA, ColsB, Prec> result(Prec(0));
   for (size_t i = 0; i < RowsA; ++i)
   {
       for (size_t j = 0; j < ColsB; ++j)
       {
           for (size_t k = 0; k < SharedSize; ++k)
           {
               result(i, j) += first(i, k) * second(k, j);
           }
       }
   }
   return result;
}
#else
template <size_t RowsA, size_t SharedSize, size_t ColsB, typename Prec>
inline typename std::enable_if<(RowsA > 1) || (ColsB > 1),
                               Matrix<RowsA, ColsB, Prec>>::type
operator*(Matrix<RowsA, SharedSize, Prec> first,
          const Matrix<SharedSize, ColsB, Prec>& second)
{
    Matrix<RowsA, ColsB, Prec> result(Prec(0));
    for (size_t i = 0; i < RowsA; ++i)
    {
        for (size_t j = 0; j < ColsB; ++j)
        {
            for (size_t k = 0; k < SharedSize; ++k)
            {
                result(i, j) += first(i, k) * second(k, j);
            }
        }
    }
    return result;
}

template <size_t RowsA, size_t SharedSize, size_t ColsB, typename Prec>
inline typename std::enable_if<(RowsA == 1) && (ColsB == 1), Prec>::type
operator*(Matrix<RowsA, SharedSize, Prec> first,
          const Matrix<SharedSize, ColsB, Prec>& second)
{
    Prec result(0);
    for (size_t k = 0; k < SharedSize; ++k)
    {
        result += first(k) * second(k);
    }
    return result;
}
#endif // KRAKEN_ALLOW_DEGENERATE_MATRICES

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>&
Matrix<Rows, Cols, Prec>::operator+=(const Matrix<Rows, Cols, Prec>& other)
{
    for (size_t i = 0; i < Rows; ++i)
    {
        for (size_t j = 0; j < Cols; ++j)
        {
            (*this)(i, j) += other(i, j);
        }
    }
    return *this;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>&
Matrix<Rows, Cols, Prec>::operator-=(const Matrix<Rows, Cols, Prec>& other)
{
    for (size_t i = 0; i < Rows; ++i)
    {
        for (size_t j = 0; j < Cols; ++j)
        {
            (*this)(i, j) -= other(i, j);
        }
    }
    return *this;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>&
Matrix<Rows, Cols, Prec>::operator*=(const Matrix<Cols, Cols, Prec>& other)
{
    return (*this = *this * other);
}

template <size_t Rows, size_t Cols, typename Prec, typename OPrec>
inline Matrix<Rows, Cols, Prec>
operator*(Matrix<Rows, Cols, Prec> mat, OPrec scalar)
{
    return mat *= scalar;
}

template <size_t Rows, size_t Cols, typename Prec, typename OPrec>
inline Matrix<Rows, Cols, Prec>
operator*(OPrec scalar, Matrix<Rows, Cols, Prec> mat)
{
    return mat *= scalar;
}

template <size_t Rows, size_t Cols, typename Prec, typename OPrec>
inline Matrix<Rows, Cols, Prec>
operator/(Matrix<Rows, Cols, Prec> mat, OPrec scalar)
{
    return mat /= scalar;
}


template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if<(Rows > 1 && Cols > 1), std::ostream&>::type
operator<<(std::ostream& ost, const Matrix<Rows, Cols, Prec>& mat)
{
    ost << "{";
    for (size_t i = 0; i < Rows; ++i)
    {
        ost << "{";
        for (size_t j = 0; j < Cols; ++j)
        {
            ost << mat(i,j);
            if (j+1 < Cols) ost << ", ";
        }
        ost << "}";
        if (i+1 < Rows) ost << ", ";
    }
    ost << "}";
    return ost;
}

template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if<Rows == 1 || Cols == 1, std::ostream&>::type
operator<<(std::ostream& ost, const Matrix<Rows, Cols, Prec>& mat)
{
    ost << "{";
    for (size_t i = 0; i < Rows*Cols; ++i)
    {
        ost << mat(i);
        if (i+1 < Rows*Cols) ost << ", ";
    }
    ost << "}";
    return ost;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Cols, Rows, Prec> transpose(const Matrix<Rows, Cols, Prec>& mat)
{
    return mat.transpose();
}

template <size_t Dim, typename Prec>
Prec trace(const Matrix<Dim, Dim, Prec>& mat)
{
    auto tr = Prec(0);
    for (size_t i = 0; i < Dim; ++i)
    {
        tr += mat(i, i);
    }
    return tr;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec> abs(const Matrix<Rows, Cols, Prec>& mat)
{
    Matrix<Rows, Cols, Prec> res;
    for (size_t i = 0; i < Rows; ++i)
    {
        for (size_t j = 0; j < Cols; ++j)
        {
            res(i, j) = std::abs(mat(i, j));
        }
    }
    return res;
}

template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if<(Rows > 1 || Cols > 1) || (Rows == 1 && Cols == 1), bool>::type
operator==(const Matrix<Rows, Cols, Prec>& a,
           const Matrix<Rows, Cols, Prec>& b)
{
    for (size_t i = 0; i < Rows*Cols; ++i)
    {
        if (a.data()[i] != b.data()[i]) return false;
    }
    return true;
}

template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if<(Rows > 1 || Cols > 1) || (Rows == 1 && Cols == 1), bool>::type
operator!=(const Matrix<Rows, Cols, Prec>& a,
           const Matrix<Rows, Cols, Prec>& b)
{
    return !(a == b);
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), bool>::type
operator==(const Matrix<Length, 1, Prec>& a,
           const Matrix<1, Length, Prec>& b)
{
    for (size_t i = 0; i < Length; ++i)
    {
        if (a.data()[i] != b.data()[i]) return false;
    }
    return true;
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), bool>::type
operator!=(const Matrix<Length, 1, Prec>& a,
           const Matrix<1, Length, Prec>& b)
{
    return !(a == b);
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), bool>::type
operator==(const Matrix<1, Length, Prec>& a,
           const Matrix<Length, 1, Prec>& b)
{
    for (size_t i = 0; i < Length; ++i)
    {
        if (a.data()[i] != b.data()[i]) return false;
    }
    return true;
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), bool>::type
operator!=(const Matrix<1, Length, Prec>& a,
           const Matrix<Length, 1, Prec>& b)
{
    return !(a == b);
}

template <size_t Rows, size_t Cols, typename Prec>
inline bool zero(const Matrix<Rows, Cols, Prec>& mat,
                 Prec epsilon = 20 * Constant<Prec>::epsilon)
{
    for (size_t i = 0; i < Rows * Cols; ++i)
    {
        if (!zero(std::abs(mat.data()[i]), epsilon)) return false;
    }
    return true;
}

template <typename NewPrec, size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, NewPrec>
prec_cast(const Matrix<Rows, Cols, Prec>& mat)
{
    Matrix<Rows, Cols, NewPrec> newmat;
    for (size_t i = 0; i < Rows*Cols; ++i)
    {
        newmat.data()[i] = static_cast<NewPrec>(mat.data()[i]);
    }
    return newmat;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, float>
lowp_cast(const Matrix<Rows, Cols, Prec>& mat)
{
    return prec_cast<float>(mat);
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, double>
highp_cast(const Matrix<Rows, Cols, Prec>& mat)
{
    return prec_cast<double>(mat);
}

////////////////////////////////////////////////////////////////////////////////
/// Vectors
////////////////////////////////////////////////////////////////////////////////
template <typename Prec>
class Matrix<2, 1, Prec>
{
public:
    union {Prec x, r, s;};
    union {Prec y, g, t;};

    Matrix() = default;
    explicit Matrix(Prec diag)
    {
        x = diag;
        y = Prec(0);
    }
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() != 2)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        x = *beg++;
        y = *beg;
    }

    Prec* data() { return &x; }
    const Prec* data() const { return &x; }

    size_t rowCount() const { return 2; }
    size_t colCount() const { return 1; }

    Prec& operator()(size_t i) { return data()[i]; }
    Prec  operator()(size_t i) const { return data()[i]; }
    Prec& operator[](size_t i) { return data()[i]; }
    Prec  operator[](size_t i) const { return data()[i]; }

    Prec operator()(size_t i, size_t) const
    {
        return data()[i];
    }

    Prec& operator()(size_t i, size_t)
    {
        return data()[i];
    }

    Prec row(size_t i) const
    {
        return data()[i];
    }

    void setRow(size_t i, Prec r)
    {
        data()[i] = r;
    }

    Matrix<2, 1, Prec>& col(size_t) const
    {
        return *this;
    }

    void setCol(size_t, const Matrix<2, 1, Prec>& c)
    {
        *this = c;
    }
    void setCol(size_t, const Matrix<1, 2, Prec>& c)
    {
        x = c(0);
        y = c(1);
    }

    Matrix<2, 1, Prec>& operator+=(const Matrix<2, 1, Prec>& other)
    {
        x += other.x;
        y += other.y;
        return *this;
    }
    Matrix<2, 1, Prec>& operator-=(const Matrix<2, 1, Prec>& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }

    template <typename OPrec>
    Matrix<2, 1, Prec>& operator*=(OPrec scalar)
    {
        x *= scalar;
        y *= scalar;
        return *this;
    }

    template <typename OPrec>
    Matrix<2, 1, Prec>& operator/=(OPrec scalar)
    {
        x /= scalar;
        y /= scalar;
        return *this;
    }

    Matrix<2, 1, Prec> operator+() const
    {
        return {+x, +y};
    }

    Matrix<2, 1, Prec> operator-() const
    {
        return {-x, -y};
    }

    Matrix<1, 2, Prec> transpose() const
    {
        return {x, y};
    }
};

template <typename Prec>
class Matrix<3, 1, Prec>
{
public:
    union {Prec x, r, s;};
    union {Prec y, g, t;};
    union {Prec z, b, p;};

    Matrix() = default;
    explicit Matrix(Prec diag)
    {
        x = diag;
        y = Prec(0);
        z = Prec(0);
    }
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() != 3)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        x = *beg++;
        y = *beg++;
        z = *beg;
    }

    Prec* data() { return &x; }
    const Prec* data() const { return &x; }

    size_t rowCount() const { return 3; }
    size_t colCount() const { return 1; }

    Prec& operator()(size_t i) { return data()[i]; }
    Prec  operator()(size_t i) const { return data()[i]; }
    Prec& operator[](size_t i) { return data()[i]; }
    Prec  operator[](size_t i) const { return data()[i]; }

    Prec operator()(size_t i, size_t) const
    {
        return data()[i];
    }

    Prec& operator()(size_t i, size_t)
    {
        return data()[i];
    }

    Prec row(size_t i) const
    {
        return data()[i];
    }

    void setRow(size_t i, Prec r)
    {
        data()[i] = r;
    }

    Matrix<3, 1, Prec>& col(size_t) const
    {
        return *this;
    }

    void setCol(size_t, const Matrix<3, 1, Prec>& c)
    {
        *this = c;
    }
    void setCol(size_t, const Matrix<1, 3, Prec>& c)
    {
        x = c(0);
        y = c(1);
        z = c(2);
    }

    Matrix<3, 1, Prec>& operator+=(const Matrix<3, 1, Prec>& other)
    {
        x += other.x;
        y += other.y;
        z += other.z;
        return *this;
    }
    Matrix<3, 1, Prec>& operator-=(const Matrix<3, 1, Prec>& other)
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return *this;
    }

    template <typename OPrec>
    Matrix<3, 1, Prec>& operator*=(OPrec scalar)
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return *this;
    }

    template <typename OPrec>
    Matrix<3, 1, Prec>& operator/=(OPrec scalar)
    {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        return *this;
    }

    Matrix<3, 1, Prec> operator+() const
    {
        return {+x, +y, +z};
    }

    Matrix<3, 1, Prec> operator-() const
    {
        return {-x, -y, -z};
    }

    Matrix<1, 3, Prec> transpose() const
    {
        return {x, y, z};
    }
};

template <typename Prec>
class Matrix<4, 1, Prec>
{
public:
    union {Prec x, r, s;};
    union {Prec y, g, t;};
    union {Prec z, b, p;};
    union {Prec w, a, q;};

    Matrix() = default;
    explicit Matrix(Prec diag)
    {
        x = diag;
        y = Prec(0);
        z = Prec(0);
        w = Prec(0);
    }
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() != 4)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        x = *beg++;
        y = *beg++;
        z = *beg++;
        w = *beg;
    }

    Prec* data() { return &x; }
    const Prec* data() const { return &x; }

    size_t rowCount() const { return 4; }
    size_t colCount() const { return 1; }

    Prec& operator()(size_t i) { return data()[i]; }
    Prec  operator()(size_t i) const { return data()[i]; }
    Prec& operator[](size_t i) { return data()[i]; }
    Prec  operator[](size_t i) const { return data()[i]; }

    Prec operator()(size_t i, size_t) const
    {
        return data()[i];
    }

    Prec& operator()(size_t i, size_t)
    {
        return data()[i];
    }

    Prec row(size_t i) const
    {
        return data()[i];
    }

    void setRow(size_t i, Prec r)
    {
        data()[i] = r;
    }

    Matrix<4, 1, Prec>& col(size_t) const
    {
        return *this;
    }

    void setCol(size_t, const Matrix<4, 1, Prec>& c)
    {
        *this = c;
    }
    void setCol(size_t, const Matrix<1, 4, Prec>& c)
    {
        x = c(0);
        y = c(1);
        z = c(2);
        w = c(3);
    }

    Matrix<4, 1, Prec>& operator+=(const Matrix<4, 1, Prec>& other)
    {
        x += other.x;
        y += other.y;
        z += other.z;
        w += other.w;
        return *this;
    }
    Matrix<4, 1, Prec>& operator-=(const Matrix<4, 1, Prec>& other)
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        w -= other.w;
        return *this;
    }

    template <typename OPrec>
    Matrix<4, 1, Prec>& operator*=(OPrec scalar)
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        w *= scalar;
        return *this;
    }

    template <typename OPrec>
    Matrix<4, 1, Prec>& operator/=(OPrec scalar)
    {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        w /= scalar;
        return *this;
    }

    Matrix<4, 1, Prec> operator+() const
    {
        return {+x, +y, +z, +w};
    }

    Matrix<4, 1, Prec> operator-() const
    {
        return {-x, -y, -z, -w};
    }

    Matrix<1, 4, Prec> transpose() const
    {
        return {x, y, z, w};
    }
};

template <size_t Length, typename Prec>
using Vector = Matrix<Length, 1, Prec>;

template <size_t Length, typename Prec>
using RowVector = Matrix<1, Length, Prec>;

////////////////////////////////////////////////////////////////////////////////
/// Vector operations
////////////////////////////////////////////////////////////////////////////////

template <size_t Length, typename Prec>
inline Prec dot(const Vector<Length, Prec>& a, const Vector<Length, Prec>& b)
{
    Prec val(0);
    for (size_t i = 0; i < Length; ++i)
    {
        val += a(i) * b(i);
    }
    return val;
}

template <size_t Length, typename Prec>
inline Prec squaredLength(const Vector<Length, Prec>& a)
{
    return dot(a, a);
}

template <size_t Length, typename Prec>
inline Prec length(const Vector<Length, Prec>& a)
{
    return std::sqrt(dot(a, a));
}

template <size_t Length, typename Prec>
inline Vector<Length, Prec> normalise(Vector<Length, Prec> a)
{
    auto mul = Prec(1) / length(a);
    return a *= mul;
}

template <size_t Length, typename Prec>
inline Vector<Length, Prec> project(const Vector<Length, Prec>& a,
                                    const Vector<Length, Prec>& b)
{
    return dot(a, b)/dot(b,b)*b;
}

template <size_t Length, typename Prec>
inline Vector<Length, Prec> projectUnit(const Vector<Length, Prec>& a,
                                        const Vector<Length, Prec>& b)
{
    return dot(a, b) * b;
}

template <typename Prec>
inline Vector<3, Prec> cross(const Vector<3, Prec>& a, const Vector<3, Prec>& b)
{
    return
    {
        a.y*b.z-a.z*b.y,
        a.z*b.x-a.x*b.z,
        a.x*b.y-a.y*b.x
    };
}

template <typename Prec>
inline Vector<2, Prec> perp(const Vector<2, Prec>& a)
{
    return {-a.y, a.x};
}

template <size_t Length, typename Prec>
inline bool parallel(const Vector<Length, Prec>& a,
                     const Vector<Length, Prec>& b,
                     Prec epsilon = 20 * Constant<Prec>::epsilon)
{
    // WARNING: This is incorrect for complex vector spaces (needs conjugation).
    // TODO: Fix that.
    auto d = dot(a, b);
    d *= d;
    auto al = dot(a, a);
    auto bl = dot(b, b);
    return d >= al*bl*(Prec(1)-epsilon);
}

////////////////////////////////////////////////////////////////////////////////
/// Row vector operations
////////////////////////////////////////////////////////////////////////////////


template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), Prec>::type
squaredLength(const RowVector<Length, Prec>& a)
{
   return dot(a, a);
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), Prec>::type
length(const RowVector<Length, Prec>& a)
{
   return std::sqrt(dot(a, a));
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), RowVector<Length, Prec>>::type
normalise(RowVector<Length, Prec> a)
{
   auto mul = Prec(1) / length(a);
   return a *= mul;
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), RowVector<Length, Prec>>::type
project(const RowVector<Length, Prec>& a, const RowVector<Length, Prec>& b)
{
   return dot(a, b)/dot(b,b)*b;
}

template <size_t Length, typename Prec>
inline typename std::enable_if<(Length > 1), Prec>::type
dot(const RowVector<Length, Prec>& a, const RowVector<Length, Prec>& b)
{
    Prec val(0);
    for (size_t i = 0; i < Length; ++i)
    {
        val += a(i) * b(i);
    }
    return val;
}


////////////////////////////////////////////////////////////////////////////////
/// Basic matrix operations
////////////////////////////////////////////////////////////////////////////////
template <typename Prec>
inline Prec det(const Matrix<2,2,Prec>& m)
{
    return m(0, 0) * m(1, 1) - m(0, 1) * m(1, 0);
}

template <typename Prec>
inline Matrix<2,2,Prec> inverse(const Matrix<2,2,Prec>& m)
{
    auto d = Prec(1)/det(m);
    Matrix<2,2,Prec> a;
    a(0, 0) =  m(1, 1)*d;
    a(0, 1) = -m(0, 1)*d;
    a(1, 0) = -m(1, 0)*d;
    a(1, 1) =  m(0, 0)*d;
    return a;
}


template <typename Prec>
inline Prec det(const Matrix<3,3,Prec>& m)
{
    return  m(0, 0)*(m(1, 1) * m(2, 2) - m(1, 2) * m(2, 1))
           -m(1, 0)*(m(0, 1) * m(2, 2) - m(2, 1) * m(0, 2))
           +m(2, 0)*(m(0, 1) * m(1, 2) - m(1, 1) * m(0, 2));
}

template <typename Prec>
inline Matrix<3,3,Prec> inverse(const Matrix<3,3,Prec>& m)
{
    Matrix<3,3,Prec> a;
    auto d00 = m(1, 1) * m(2, 2) - m(1, 2) * m(2, 1);
    auto d10 = m(2, 1) * m(0, 2) - m(0, 1) * m(2, 2);
    auto d20 = m(0, 1) * m(1, 2) - m(1, 1) * m(0, 2);

    auto d = Prec(1)/(m(0, 0) * d00 + m(1, 0) * d10 + m(2, 0) * d20);
    a(0, 0) = d00 * d;
    a(1, 0) = (m(2, 0) * m(1, 2) - m(1, 0) * m(2, 2)) * d;
    a(2, 0) = (m(1, 0) * m(2, 1) - m(2, 0) * m(1, 1)) * d;
    a(0, 1) = d10 * d;
    a(1, 1) = (m(0, 0) * m(2, 2) - m(2, 0) * m(0, 2)) * d;
    a(2, 1) = (m(2, 0) * m(0, 1) - m(0, 0) * m(2, 1)) * d;
    a(0, 2) = d20 * d;
    a(1, 2) = (m(1, 0) * m(0, 2) - m(0, 0) * m(1, 2)) * d;
    a(2, 2) = (m(0, 0) * m(1, 1) - m(1, 0) * m(0, 1)) * d;
    return a;
}

// Are the below too complicated / would it be quicker or more stable
// numerically to just use the general algorithms?
template <typename Prec>
inline Prec det(const Matrix<4,4,Prec>& m)
{
    auto d00 =  m(1, 1)*m(2, 2)*m(3, 3)-m(1, 1)*m(3, 2)*m(2, 3)
               -m(2, 1)*m(1, 2)*m(3, 3)+m(2, 1)*m(3, 2)*m(1, 3)
               +m(3, 1)*m(1, 2)*m(2, 3)-m(3, 1)*m(2, 2)*m(1, 3);
    auto d01 =  m(1, 0)*m(3, 2)*m(2, 3)-m(1, 0)*m(2, 2)*m(3, 3)
               +m(2, 0)*m(1, 2)*m(3, 3)-m(2, 0)*m(3, 2)*m(1, 3)
               -m(3, 0)*m(1, 2)*m(2, 3)+m(3, 0)*m(2, 2)*m(1, 3);
    auto d02 =  m(1, 0)*m(2, 1)*m(3, 3)-m(1, 0)*m(3, 1)*m(2, 3)
               -m(2, 0)*m(1, 1)*m(3, 3)+m(2, 0)*m(3, 1)*m(1, 3)
               +m(3, 0)*m(1, 1)*m(2, 3)-m(3, 0)*m(2, 1)*m(1, 3);
    auto d03 =  m(1, 0)*m(3, 1)*m(2, 2)-m(1, 0)*m(2, 1)*m(3, 2)
               +m(2, 0)*m(1, 1)*m(3, 2)-m(2, 0)*m(3, 1)*m(1, 2)
               -m(3, 0)*m(1, 1)*m(2, 2)+m(3, 0)*m(2, 1)*m(1, 2);
    return m(0, 0) * d00 + m(0, 1) * d01 + m(0, 2) * d02 + m(0, 3) * d03;
}

template <typename Prec>
inline Matrix<4,4,Prec> inverse(const Matrix<4,4,Prec>& m)
{
    auto d00 =  m(1, 1)*m(2, 2)*m(3, 3)-m(1, 1)*m(3, 2)*m(2, 3)
               -m(2, 1)*m(1, 2)*m(3, 3)+m(2, 1)*m(3, 2)*m(1, 3)
               +m(3, 1)*m(1, 2)*m(2, 3)-m(3, 1)*m(2, 2)*m(1, 3);
    auto d01 =  m(1, 0)*m(3, 2)*m(2, 3)-m(1, 0)*m(2, 2)*m(3, 3)
               +m(2, 0)*m(1, 2)*m(3, 3)-m(2, 0)*m(3, 2)*m(1, 3)
               -m(3, 0)*m(1, 2)*m(2, 3)+m(3, 0)*m(2, 2)*m(1, 3);
    auto d02 =  m(1, 0)*m(2, 1)*m(3, 3)-m(1, 0)*m(3, 1)*m(2, 3)
               -m(2, 0)*m(1, 1)*m(3, 3)+m(2, 0)*m(3, 1)*m(1, 3)
               +m(3, 0)*m(1, 1)*m(2, 3)-m(3, 0)*m(2, 1)*m(1, 3);
    auto d03 =  m(1, 0)*m(3, 1)*m(2, 2)-m(1, 0)*m(2, 1)*m(3, 2)
               +m(2, 0)*m(1, 1)*m(3, 2)-m(2, 0)*m(3, 1)*m(1, 2)
               -m(3, 0)*m(1, 1)*m(2, 2)+m(3, 0)*m(2, 1)*m(1, 2);
    auto d = Prec(1)/(m(0, 0) * d00 + m(0, 1) * d01
                      + m(0, 2) * d02 + m(0, 3) * d03);
    Matrix<4,4,Prec> a;
    a(0, 0) = d00;
    a(1, 0) = d01;
    a(2, 0) = d02;
    a(3, 0) = d03;

    a(0, 1) =  m(0, 1)*m(3, 2)*m(2, 3)-m(0, 1)*m(2, 2)*m(3, 3)
              +m(2, 1)*m(0, 2)*m(3, 3)-m(2, 1)*m(3, 2)*m(0, 3)
              -m(3, 1)*m(0, 2)*m(2, 3)+m(3, 1)*m(2, 2)*m(0, 3);
    a(0, 2) =  m(0, 1)*m(1, 2)*m(3, 3)-m(0, 1)*m(3, 2)*m(1, 3)
              -m(1, 1)*m(0, 2)*m(3, 3)+m(1, 1)*m(3, 2)*m(0, 3)
              +m(3, 1)*m(0, 2)*m(1, 3)-m(3, 1)*m(1, 2)*m(0, 3);
    a(0, 3) =  m(0, 1)*m(2, 2)*m(1, 3)-m(0, 1)*m(1, 2)*m(2, 3)
              +m(1, 1)*m(0, 2)*m(2, 3)-m(1, 1)*m(2, 2)*m(0, 3)
              -m(2, 1)*m(0, 2)*m(1, 3)+m(2, 1)*m(1, 2)*m(0, 3);

    a(1, 1) =  m(0, 0)*m(2, 2)*m(3, 3)-m(0, 0)*m(3, 2)*m(2, 3)
              -m(2, 0)*m(0, 2)*m(3, 3)+m(2, 0)*m(3, 2)*m(0, 3)
              +m(3, 0)*m(0, 2)*m(2, 3)-m(3, 0)*m(2, 2)*m(0, 3);
    a(1, 2) =  m(0, 0)*m(3, 2)*m(1, 3)-m(0, 0)*m(1, 2)*m(3, 3)
              +m(1, 0)*m(0, 2)*m(3, 3)-m(1, 0)*m(3, 2)*m(0, 3)
              -m(3, 0)*m(0, 2)*m(1, 3)+m(3, 0)*m(1, 2)*m(0, 3);
    a(1, 3) =  m(0, 0)*m(1, 2)*m(2, 3)-m(0, 0)*m(2, 2)*m(1, 3)
              -m(1, 0)*m(0, 2)*m(2, 3)+m(1, 0)*m(2, 2)*m(0, 3)
              +m(2, 0)*m(0, 2)*m(1, 3)-m(2, 0)*m(1, 2)*m(0, 3);

    a(2, 1) =  m(0, 0)*m(3, 1)*m(2, 3)-m(0, 0)*m(2, 1)*m(3, 3)
              +m(2, 0)*m(0, 1)*m(3, 3)-m(2, 0)*m(3, 1)*m(0, 3)
              -m(3, 0)*m(0, 1)*m(2, 3)+m(3, 0)*m(2, 1)*m(0, 3);
    a(2, 2) =  m(0, 0)*m(1, 1)*m(3, 3)-m(0, 0)*m(3, 1)*m(1, 3)
              -m(1, 0)*m(0, 1)*m(3, 3)+m(1, 0)*m(3, 1)*m(0, 3)
              +m(3, 0)*m(0, 1)*m(1, 3)-m(3, 0)*m(1, 1)*m(0, 3);
    a(2, 3) =  m(0, 0)*m(2, 1)*m(1, 3)-m(0, 0)*m(1, 1)*m(2, 3)
              +m(1, 0)*m(0, 1)*m(2, 3)-m(1, 0)*m(2, 1)*m(0, 3)
              -m(2, 0)*m(0, 1)*m(1, 3)+m(2, 0)*m(1, 1)*m(0, 3);

    a(3, 1) =  m(0, 0)*m(2, 1)*m(3, 2)-m(0, 0)*m(3, 1)*m(2, 2)
              -m(2, 0)*m(0, 1)*m(3, 2)+m(2, 0)*m(3, 1)*m(0, 2)
              +m(3, 0)*m(0, 1)*m(2, 2)-m(3, 0)*m(2, 1)*m(0, 2);
    a(3, 2) =  m(0, 0)*m(3, 1)*m(1, 2)-m(0, 0)*m(1, 1)*m(3, 2)
              +m(1, 0)*m(0, 1)*m(3, 2)-m(1, 0)*m(3, 1)*m(0, 2)
              -m(3, 0)*m(0, 1)*m(1, 2)+m(3, 0)*m(1, 1)*m(0, 2);
    a(3, 3) =  m(0, 0)*m(1, 1)*m(2, 2)-m(0, 0)*m(2, 1)*m(1, 2)
              -m(1, 0)*m(0, 1)*m(2, 2)+m(1, 0)*m(2, 1)*m(0, 2)
              +m(2, 0)*m(0, 1)*m(1, 2)-m(2, 0)*m(1, 1)*m(0, 2);
    return a * d;
}

////////////////////////////////////////////////////////////////////////////////
/// Typedefs
////////////////////////////////////////////////////////////////////////////////
using  vec2 = Vector<2,F32>;
using dvec2 = Vector<2,F64>;
using ivec2 = Vector<2,S32>;
using uvec2 = Vector<2,U32>;
using  vec3 = Vector<3,F32>;
using dvec3 = Vector<3,F64>;
using ivec3 = Vector<3,S32>;
using uvec3 = Vector<3,U32>;
using  vec4 = Vector<4,F32>;
using dvec4 = Vector<4,F64>;
using ivec4 = Vector<4,S32>;
using uvec4 = Vector<4,U32>;

using  mat2 = Matrix<2,2,F32>;
using dmat2 = Matrix<2,2,F64>;
using imat2 = Matrix<2,2,S32>;
using umat2 = Matrix<2,2,U32>;
using  mat3 = Matrix<3,3,F32>;
using dmat3 = Matrix<3,3,F64>;
using imat3 = Matrix<3,3,S32>;
using umat3 = Matrix<3,3,U32>;
using  mat4 = Matrix<4,4,F32>;
using dmat4 = Matrix<4,4,F64>;
using imat4 = Matrix<4,4,S32>;
using umat4 = Matrix<4,4,U32>;

////////////////////////////////////////////////////////////////////////////////
/// Static assertions
////////////////////////////////////////////////////////////////////////////////
// Assert that assumptions about the type are correct at compile-time:
// * The sizes are correct.
// * Arrays are tightly packed / there is no padding.
static_assert(std::is_pod< vec2>::value,  "vec2 must be POD");
static_assert(std::is_pod<dvec2>::value, "dvec2 must be POD");
static_assert(std::is_pod<ivec2>::value, "ivec2 must be POD");
static_assert(std::is_pod<uvec2>::value, "uvec2 must be POD");
static_assert(sizeof( vec2) == 4*2,   "Size of vec2 is wrong.");
static_assert(sizeof(dvec2) == 8*2, "Size of dvec2 is wrong.");
static_assert(sizeof(ivec2) == 4*2, "Size of ivec2 is wrong.");
static_assert(sizeof(uvec2) == 4*2, "Size of uvec2 is wrong.");
static_assert(sizeof( vec2[2]) == sizeof( vec2)*2,  "vec2 not tightly packed.");
static_assert(sizeof(dvec2[2]) == sizeof(dvec2)*2, "dvec2 not tightly packed.");
static_assert(sizeof(ivec2[2]) == sizeof(ivec2)*2, "ivec2 not tightly packed.");
static_assert(sizeof(uvec2[2]) == sizeof(uvec2)*2, "uvec2 not tightly packed.");

static_assert(std::is_pod< vec3>::value,  "vec3 must be POD");
static_assert(std::is_pod<dvec3>::value, "dvec3 must be POD");
static_assert(std::is_pod<ivec3>::value, "ivec3 must be POD");
static_assert(std::is_pod<uvec3>::value, "uvec3 must be POD");
static_assert(sizeof( vec3) == 4*3, "Size of vec3 is wrong.");
static_assert(sizeof(dvec3) == 8*3, "Size of dvec3 is wrong.");
static_assert(sizeof(ivec3) == 4*3, "Size of ivec3 is wrong.");
static_assert(sizeof(uvec3) == 4*3, "Size of uvec3 is wrong.");
static_assert(sizeof( vec3[2]) == sizeof( vec3)*2,  "vec3 not tightly packed.");
static_assert(sizeof(dvec3[2]) == sizeof(dvec3)*2, "dvec3 not tightly packed.");
static_assert(sizeof(ivec3[2]) == sizeof(ivec3)*2, "ivec3 not tightly packed.");
static_assert(sizeof(uvec3[2]) == sizeof(uvec3)*2, "uvec3 not tightly packed.");

static_assert(std::is_pod< vec4>::value,  "vec4 must be POD");
static_assert(std::is_pod<dvec4>::value, "dvec4 must be POD");
static_assert(std::is_pod<ivec4>::value, "ivec4 must be POD");
static_assert(std::is_pod<uvec4>::value, "uvec4 must be POD");
static_assert(sizeof( vec4) == 4*4, "Size of vec4 is wrong.");
static_assert(sizeof(dvec4) == 8*4, "Size of dvec4 is wrong.");
static_assert(sizeof(ivec4) == 4*4, "Size of ivec4 is wrong.");
static_assert(sizeof(uvec4) == 4*4, "Size of uvec4 is wrong.");
static_assert(sizeof( vec4[2]) == sizeof( vec4)*2,  "vec4 not tightly packed.");
static_assert(sizeof(dvec4[2]) == sizeof(dvec4)*2, "dvec4 not tightly packed.");
static_assert(sizeof(ivec4[2]) == sizeof(ivec4)*2, "ivec4 not tightly packed.");
static_assert(sizeof(uvec4[2]) == sizeof(uvec4)*2, "uvec4 not tightly packed.");

static_assert(std::is_pod< mat2>::value,  "mat2 must be POD");
static_assert(std::is_pod<dmat2>::value, "dmat2 must be POD");
static_assert(std::is_pod<imat2>::value, "imat2 must be POD");
static_assert(std::is_pod<umat2>::value, "umat2 must be POD");
static_assert(sizeof( mat2) == sizeof( vec2)*2,  "Size of mat2 is wrong.");
static_assert(sizeof(dmat2) == sizeof(dvec2)*2, "Size of dmat2 is wrong.");
static_assert(sizeof(imat2) == sizeof(ivec2)*2, "Size of imat2 is wrong.");
static_assert(sizeof(umat2) == sizeof(uvec2)*2, "Size of umat2 is wrong.");
static_assert(sizeof( mat2[2]) == sizeof( mat2)*2,  "mat2 not tightly packed.");
static_assert(sizeof(dmat2[2]) == sizeof(dmat2)*2, "dmat2 not tightly packed.");
static_assert(sizeof(imat2[2]) == sizeof(imat2)*2, "imat2 not tightly packed.");
static_assert(sizeof(umat2[2]) == sizeof(umat2)*2, "umat2 not tightly packed.");

static_assert(std::is_pod< mat3>::value, " mat3 must be POD");
static_assert(std::is_pod<dmat3>::value, "dmat3 must be POD");
static_assert(std::is_pod<imat3>::value, "imat3 must be POD");
static_assert(std::is_pod<umat3>::value, "umat3 must be POD");
static_assert(sizeof( mat3) == sizeof( vec3)*3,  "Size of mat3 is wrong.");
static_assert(sizeof(dmat3) == sizeof(dvec3)*3, "Size of dmat3 is wrong.");
static_assert(sizeof(imat3) == sizeof(ivec3)*3, "Size of imat3 is wrong.");
static_assert(sizeof(umat3) == sizeof(uvec3)*3, "Size of umat3 is wrong.");
static_assert(sizeof( mat3[2]) == sizeof( mat3)*2,  "mat3 not tightly packed.");
static_assert(sizeof(dmat3[2]) == sizeof(dmat3)*2, "dmat3 not tightly packed.");
static_assert(sizeof(imat3[2]) == sizeof(imat3)*2, "imat3 not tightly packed.");
static_assert(sizeof(umat3[2]) == sizeof(umat3)*2, "umat3 not tightly packed.");

static_assert(std::is_pod< mat4>::value,  "mat4 must be POD");
static_assert(std::is_pod<dmat4>::value, "dmat4 must be POD");
static_assert(std::is_pod<imat4>::value, "imat4 must be POD");
static_assert(std::is_pod<umat4>::value, "umat4 must be POD");
static_assert(sizeof( mat4) == sizeof( vec4)*4,  "Size of mat4 is wrong.");
static_assert(sizeof(dmat4) == sizeof(dvec4)*4, "Size of dmat4 is wrong.");
static_assert(sizeof(imat4) == sizeof(ivec4)*4, "Size of imat4 is wrong.");
static_assert(sizeof(umat4) == sizeof(uvec4)*4, "Size of umat4 is wrong.");
static_assert(sizeof( mat4[2]) == sizeof( mat4)*2,  "mat4 not tightly packed.");
static_assert(sizeof(dmat4[2]) == sizeof(dmat4)*2, "dmat4 not tightly packed.");
static_assert(sizeof(imat4[2]) == sizeof(imat4)*2, "imat4 not tightly packed.");
static_assert(sizeof(umat4[2]) == sizeof(umat4)*2, "umat4 not tightly packed.");

} // namespace math

} // namespace kraken

#endif // KRAKEN_MATH_MATRIX_HPP_INCLUDED
