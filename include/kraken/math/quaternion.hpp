// This file is part of the Kraken project, and in addition to the license for
// that project (if any), this file itself is also available under the MIT
// License, the text of which follows below.

// MIT License

// Copyright (c) 2022 Thor Gabelgaard Nielsen

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef KRAKEN_MATH_QUATERNION_HPP_INCLUDED
#define KRAKEN_MATH_QUATERNION_HPP_INCLUDED

#include <kraken/math/comparison.hpp>

#include <kraken/types.hpp>

#include <ostream>

namespace kraken
{

namespace math
{

template <typename T>
struct quat_t;

// quat * quat
template <typename T>
inline quat_t<T> operator*(quat_t<T> a, const quat_t<T>& b);

template <typename T>
struct quat_t
{
    T w;
    T x;
    T y;
    T z;

    inline T& operator[](size_t n)
    {
        return (&w)[n];
    }

    inline const T& operator[](size_t n) const
    {
        return (&w)[n];
    }

    inline T& operator()(size_t n)
    {
        return (&w)[n];
    }

    inline const T& operator()(size_t n) const
    {
        return (&w)[n];
    }

    inline quat_t& operator+=(const quat_t& o)
    {
        w += o.w;
        x += o.x;
        y += o.y;
        z += o.z;
        return *this;
    }

    inline quat_t& operator-=(const quat_t& o)
    {
        w -= o.w;
        x -= o.x;
        y -= o.y;
        z -= o.z;
        return *this;
    }

    inline quat_t& operator*=(const quat_t& o)
    {
        *this = *this * o;
        return *this;
    }

    inline quat_t& operator*=(const T& s)
    {
        w *= s;
        x *= s;
        y *= s;
        z *= s;
        return *this;
    }

    inline quat_t& operator/=(const T& s)
    {
        w /= s;
        x /= s;
        y /= s;
        z /= s;
        return *this;
    }

    // Equality operators
    inline bool operator==(const quat_t& o) const
    {
        return (flt_eq(x, o.x) && flt_eq(y, o.y) &&
                flt_eq(z, o.z) && flt_eq(w, o.w));
    }

    inline bool operator!=(const quat_t& o) const
    {
        return !(*this == o);
    }
};

template <typename T>
inline quat_t<T> operator+(quat_t<T> a, const quat_t<T>& b)
{
    return a += b;
}

template <typename T>
inline quat_t<T> operator-(quat_t<T> a, const quat_t<T>& b)
{
    return a -= b;
}

template <typename T>
inline quat_t<T> operator*(quat_t<T> a, const quat_t<T>& b)
{
    quat_t<T> q;
    q.w = a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z;
    q.x = a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y;
    q.y = a.w * b.y - a.x * b.z + a.y * b.w + a.z * b.x;
    q.z = a.w * b.z + a.x * b.y - a.y * b.x + a.z * b.w;
    return q;
}

template <typename T>
inline quat_t<T> operator*(quat_t<T> q, const T& s)
{
    return q *= s;
}

template <typename T>
inline quat_t<T> operator*(const T& s, quat_t<T> q)
{
    return q *= s;
}

template <typename T>
inline quat_t<T> operator/(quat_t<T> q, const T& s)
{
    return q /= s;
}

template <typename T>
inline T re(const quat_t<T>& q)
{
    return q.w;
}

/*
template <typename T>
inline vec3_t<T> im(const quat_t<T>& q)
{
    return {q.x, q.y, q.z};
}*/

template <typename T>
inline quat_t<T> conj(const quat_t<T>& q)
{
    return {q.w, -q.x, -q.y, -q.z};
}

template <typename T>
inline T dot(const quat_t<T>& a, const quat_t<T>& b)
{
    return a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z;
}

template <typename T>
inline T length(const quat_t<T>& q)
{
    return std::sqrt(dot(q, q));
}

template <typename T>
inline T squaredLength(const quat_t<T>& v)
{
    return dot(v, v);
}

template <typename T>
inline quat_t<T> normalise(const quat_t<T>& q)
{
    T inv_len = T(1) / length(q);
    return q * inv_len;
}


template <typename T>
inline quat_t<T> inverse(const quat_t<T>& q)
{
    T inv_len_sqr = T(1) / dot(q, q);
    return conj(q) * inv_len_sqr;
}
/*
template <typename T>
inline quat_t<T> rotation(const vec3_t<T>& axis, T angle)
{
    quat_t<T> q;
    angle *= T(.5);
    q.w = std::cos(angle);
    T s = std::sin(angle);
    q.x = axis.x * s;
    q.y = axis.y * s;
    q.z = axis.z * s;
    return q;
}*/

template <typename T>
inline T yaw(const quat_t<T>& q)
{
    return std::asin(clamp(T(2) * (q.w * q.x - q.z * q.y), T(-1), T(1)));
}

template <typename T>
inline T roll(const quat_t<T>& q)
{
    return std::atan2(T(2) * (q.w * q.z + q.x * q.y),
                      q.w * q.w - q.x * q.x + q.y * q.y - q.z * q.z);
}

template <typename T>
inline T pitch(const quat_t<T>& q)
{
    return std::atan2(T(2) * (q.w * q.y + q.x * q.z),
                      q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z);
}
/*
template <typename T>
inline vec3_t<T> euler(const quat_t<T>& q)
{
    return {pitch(q), yaw(q), roll(q)};
}

template <typename T>
inline quat_t<T> quaternion(const vec3_t<T>& v)
{
    return {T(0), v.x, v.y, v.z};
}

template <typename T>
inline vec3_t<T> rotate(const vec3_t<T>& v, const quat_t<T>& q)
{
    return im(q * quaternion(v) * conj(q));
}

template <typename T>
inline vec3_t<T> rotate(const quat_t<T>& q, const vec3_t<T>& v)
{
    return rotate(v, q);
}*/

template <typename T>
inline quat_t<T> abs(const quat_t<T>& v)
{
    return {std::abs(v.w), std::abs(v.x), std::abs(v.y), std::abs(v.z)};
}

template <typename T>
inline bool zero(const quat_t<T>& v, T epsilon)
{
    return zero(dot(v, v), epsilon*epsilon);
}

template <typename T>
inline bool zero(const quat_t<T>& v)
{
    return zero(v, 100*Constant<T>::epsilon);
}

template <typename T>
inline std::ostream& operator<<(std::ostream& ost, const quat_t<T>& q)
{
    ost << "{" << q.w << ", " << q.x << ", " << q.y << ", " << q.z << "}";
    return ost;
}

// Typedefs
using quat = quat_t<F32>;

// Assert that assumptions about the type are correct at compile-time:
// * Elements are in consecutive order.
// * Arrays are tightly packed.
// * There is no padding.
static_assert(std::is_pod<quat>::value, "quat must be POD");
static_assert(sizeof(quat) == sizeof(F32)*4, "Size of quat is wrong.");
static_assert(sizeof(quat[2]) == sizeof(quat)*2, "quat not tightly packed.");

template <typename T>
quat lowp_cast(const quat_t<T>& o)
{
    return {static_cast<float>(o.w), static_cast<float>(o.x),
            static_cast<float>(o.y), static_cast<float>(o.z)};
}

template <typename T>
quat_t<double> highp_cast(const quat_t<T>& o)
{
    return {static_cast<double>(o.w), static_cast<double>(o.x),
            static_cast<double>(o.y), static_cast<double>(o.z)};
}

} // namespace math

} // namespace kraken

#endif // KRAKEN_MATH_QUATERNION_HPP_INCLUDED
