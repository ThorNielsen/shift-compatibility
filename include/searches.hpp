#ifndef SEARCHES_HPP_INCLUDED
#define SEARCHES_HPP_INCLUDED

#include "types.hpp"

extern std::atomic<U64> g_count;
extern std::atomic<U64> g_maxCount;

struct StatWriter
{
    ~StatWriter()
    {
        std::cout << count << " " << max << "\n";
    }

    U32 count;
    U32 max;
};

// This is the main loop of the exhaustive search. While not the most efficient
// (it exhausts all possible options without using any kind of dynamic
// programming or backtracking schemes), it is quite simple and thus easy to
// understand.
template <U32 M>
void exhaustiveSESearch(DualPathIsomorphism& phi_R,
                        DualPathIsomorphism& phi_S,
                        const SearchData<M>& data,
                        DualIsomorphismEntry** entries,
                        U32 entryCount)
{
    if (!entryCount)
    {
        ++g_count;
        if (g_count % 50000 == 0)
        {
            U64 pct = ((1000*g_count)/g_maxCount + 5) / 10;
            std::cerr << g_count << "/" << g_maxCount << " (" << pct << "%)\n";
            //std::cout << phi_R << ",\n" << phi_S << "\n\n\n";
        }
        if (arePhiMapsCompatible<M>(phi_R, phi_S,
                                    *data.multistepPathsA,
                                    *data.multistepPathsB,
                                    *data.psiMultistepPathsA,
                                    *data.psiMultistepPathsB))
        {
            throw CompatibleIsomorphismsFound{};
            //std::cout << "Compatible found:\nphi_R:\n" << phi_R << "\nphi_S:\n" << phi_S << "\n\n";
        }
    }
    else
    {
        auto origPerm = entries[0]->permutation;
        do
        {
            exhaustiveSESearch(phi_R, phi_S, data, entries+1, entryCount-1);
            entries[0]->permutation.next();
        } while (entries[0]->permutation != origPerm);
    }
}

// This function sets up the main search loop and catches the exception thrown
// when compatible isomorphisms are found, in which case it returns the
// isomorphisms as they are currently compatible.
template <U32 M>
bool exhaustiveSESearch(DualPathIsomorphism& phi_R,
                        DualPathIsomorphism& phi_S,
                        const SearchData<M>& data)
{
    try
    {
        g_maxCount = 1;
        g_count = 0;

        auto commonSize = std::min(phi_R.entries.size(), phi_S.entries.size());
        std::vector<DualIsomorphismEntry*> entries;
        for (U32 i = 0; i < commonSize; ++i)
        {
            entries.push_back(&phi_R.entries[i]);
            entries.push_back(&phi_S.entries[i]);
        }

        for (U32 i = commonSize; i < phi_R.entries.size(); ++i)
        {
            entries.push_back(&phi_R.entries[i]);
        }
        for (U32 i = commonSize; i < phi_S.entries.size(); ++i)
        {
            entries.push_back(&phi_S.entries[i]);
        }
        for (auto entry : entries)
        {
            g_maxCount = g_maxCount * fac(entry->permutation.size());
        }
        exhaustiveSESearch(phi_R, phi_S, data, entries.data(), entries.size());
    }
    catch (CompatibleIsomorphismsFound)
    {
        return true;
    }
    return false;
}

#endif // SEARCHES_HPP_INCLUDED
