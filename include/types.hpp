#ifndef TYPES_HPP_INCLUDED
#define TYPES_HPP_INCLUDED

// We want to use 1x1 matrices and not have operations automatically convert
// them into scalars.
#define KRAKEN_ALLOW_DEGENERATE_MATRICES
#include <kraken/globaltypes.hpp>
#include <kraken/math/matrix.hpp>
#include <atomic>
#include <iostream>
#include <numeric>
#include <map>
#include <type_traits>

using namespace kraken::math;

// Class representing a permutation. Since this is a permutation of the
// nonnegative integers, we just store an array of nonnegative integers, where
// the position determines the mapped value.
// As an example if we have a permutation like
// 0 1 2 3 4 5
// 4 2 1 3 5 0
// (where 0 maps to 4, 1 maps to 2, 2 maps to 1, 3 to 3, etc.),
// we only store the lower row with the upper row implicit.
struct Permutation
{
    constexpr static U16 invalidEntry = 0xffff;

    // This allows us to write expressions like "perm[3]" to get the value that
    // 3 maps to.
    U16& operator[](size_t index) { return order[index]; }
    const U16& operator[](size_t index) const { return order[index]; }

    // Number of elements in permutation; note that we are zero-indexing, so the
    // elements are 0, 1, ..., size()-1.
    U16 size() const { return static_cast<U16>(order.size()); }

    void resize(U16 newSize)
    {
        auto oldSize = size();
        order.resize(newSize);
        if (oldSize < newSize)
            std::iota(order.begin()+oldSize, order.end(), oldSize);
    }

    // This constructs an identity permutation of length newSize,
    // i.e. the one which maps 0->0, 1->1, 2->2, ..., newSize->newSize.
    static Permutation identity(U16 newSize)
    {
        Permutation p;
        p.resize(newSize);
        return p;
    }

    // Returns the inverse of this permutation, but does not modify the one this
    // is called on.
    Permutation inverse() const
    {
        Permutation other;
        other.order.resize(order.size());
        for (U32 i = 0; i < order.size(); ++i)
        {
            other.order[order[i]] = i;
        }
        return other;
    }

    // Modifies this permutation to be the "next" one in some sense (the only
    // guarantee for this next algorithm is that for any permutation of length
    // n, calling next() n! times gives n! unique permutations with the next
    // call giving the original permutation back).
    // Note that even though a reference to a permutation is returned it is to
    // this very one -- the original permutation is destroyed by this function,
    // so be sure to copy the original permutation if needed as there's no going
    // back (easily).
    Permutation& next()
    {
        std::next_permutation(order.begin(), order.end());
        return *this;
    }

    // The actual order of the permutation, it is the responsibility of anything
    // modifying this that it does not become invalid (i.e. one must ensure that
    // order contains the numbers 0,...,n-1 exactly once each).
    std::vector<U16> order;
};

// This is to enable comparing permutations with "==" instead of having to call
// a utility function every time.
inline bool operator==(const Permutation& a, const Permutation& b)
{
    if (a.size() != b.size()) return false;
    for (U16 i = 0; i < a.size(); ++i)
    {
        if (a[i] != b[i]) return false;
    }
    return true;
}

// This enables comparing permutations for inequality using "!=".
inline bool operator!=(const Permutation& a, const Permutation& b)
{
    return !(a == b);
}

// This class is extremely important; it represents a path of arbitrary length,
// yet not entirely arbitrary, as the length must be known at compile time.
// The decision to make the path length a template is to enable all other code
// to know exactly how large a path is in memory, and thus avoid any kind of
// dynamic memory allocation within each Path, and thus store everything in a
// tightly packed array. This makes the data structures highly cache-friendly,
// and thus much faster than many other options.
template <U32 EdgeCount>
struct Path
{
    // The actual edges and vertices are just numbers; to use this, the vertices
    // and edges this represents paths for should obviously already have a
    // consistent (and fixed!) numbering of vertices and edges.
    // While one could technically get by storing only the edges, it has a lot
    // of computational advantages to also store the vertices, since one then
    // doesn't have to look up any edge data structure to find out whether two
    // paths can be composed, a subpath replaced, or similar.
    Vector<EdgeCount, U32> edges;
    Vector<EdgeCount+1, U32> vertices;

    // Extract a subpath at position 'begin' and of length SubCount.
    template <U32 SubCount>
    Path<SubCount> subpath(U32 begin) const
    {
        Path<SubCount> sp;
        for (U32 i = 0; i < SubCount; ++i)
        {
            sp.vertices[i] = vertices[i+begin];
            sp.edges[i] = edges[i+begin];
        }
        sp.vertices[SubCount] = vertices[begin+SubCount];
        return sp;
    }

    // Partially replaces this path with something else, i.e. does an invasive
    // overwrite. Note that it is not checked here whether the vertex endpoints
    // that form the path are compatible, in the sense that one could
    // technically make an invalid path by starting with a valid path, and then
    // replacing a subpath with some completely different vertices.
    template <U32 SubCount>
    Path<EdgeCount>& replace(const Path<SubCount>& repl, U32 beginAt)
    {
        for (U32 i = 0; i < SubCount; ++i)
        {
            vertices[i+beginAt] = repl.vertices[i];
            edges[i+beginAt] = repl.edges[i];
        }
        vertices[beginAt+SubCount] = repl.vertices[SubCount];
        return *this;
    }
};

// Error class to be able to nicely report and catch errors where paths are not
// composable due to them not sharing common vertices.
class NoCommonVertexError : std::logic_error
{
public:
    NoCommonVertexError(const std::string& msg) : std::logic_error{msg} {}
};

// Enables concatenation of paths by just using the symbol "+".
// It is checked that the end vertex of the first is identical to the start
// vertex of the second, otherwise an appropriate exception is thrown.
template <U32 EC1, U32 EC2>
Path<EC1+EC2> operator+(const Path<EC1>& p1,
                        const Path<EC2>& p2)
{
    if (p1.vertices[EC1] != p2.vertices[0])
    {
        throw NoCommonVertexError("Paths do not share a common vertex.");
    }
    Path<EC1+EC2> p;
    for (U32 i = 0; i < EC1; ++i)
    {
        p.vertices[i] = p1.vertices[i];
        p.edges[i] = p1.edges[i];
    }
    for (U32 i = 0; i < EC2; ++i)
    {
        p.vertices[EC1+i] = p2.vertices[i];
        p.edges[EC1+i] = p2.edges[i];
    }
    p.vertices[EC1+EC2] = p2.vertices[EC2];
    return p;
}

// Compare a path as lesser than another. The order is somewhat arbitrary, but
// it is consistent and thus enables usage in sorted containers, thus allowing
// for fast lookups of paths.
template <U32 EdgeCount>
bool operator<(const Path<EdgeCount>& a, const Path<EdgeCount>& b)
{
    for (U32 i = 0; i < EdgeCount; ++i)
    {
        if (a.vertices[i] < b.vertices[i]) return true;
        if (a.vertices[i] > b.vertices[i]) return false;
        if (a.edges[i] < b.edges[i]) return true;
        if (a.edges[i] > b.edges[i]) return false;
    }
    return a.vertices[EdgeCount] < b.vertices[EdgeCount];
}

template <U32 EdgeCount>
bool operator==(const Path<EdgeCount>& a, const Path<EdgeCount>& b)
{
    for (U32 i = 0; i < EdgeCount; ++i)
    {
        if (a.vertices[i] != b.vertices[i]) return false;
        if (a.edges[i] != b.edges[i]) return false;
    }
    return a.vertices[EdgeCount] == b.vertices[EdgeCount];
}

template <U32 EdgeCount>
bool operator!=(const Path<EdgeCount>& a, const Path<EdgeCount>& b)
{
    return !(a == b);
}

// This class is one of many implementations of a path isomorphism -- this is no
// longer used, but is kept as an 'easy' and non-optimised implementation to
// enable better understanding of the more complex versions.
template <U32 SrcEdges, U32 DstEdges>
struct StupidPathIsomorphism
{
    using InternalMap = std::map<Path<SrcEdges>, Path<DstEdges>>;
    using Reference = typename InternalMap::iterator;
    using ConstReference = typename InternalMap::const_iterator;

    // Checks whether this can map a given path (useful for e.g. partially
    // constructed path isomorphisms).
    bool hasEntry(const Path<SrcEdges>& path) const
    {
        return m_map.find(path) != m_map.end();
    }

    // Does what it says on the tin -- performs the actual mapping.
    Path<DstEdges> map(const Path<SrcEdges>& path)
    {
        return m_map[path];
    }

    // This adds the mapping "src \mapsto dst" to this isomorphism.
    // It returns a reference to where that mapping was internally inserted,
    // along with a boolean value determining whether the mapping already
    // existed or not.
    std::pair<Reference, bool> insert(const Path<SrcEdges>& src, const Path<DstEdges>& dst)
    {
        return m_map.insert({src, dst});
    }

    // Various ways of removing a given mapping from this. Note that one does
    // NOT need to specify the entire mapping, but only the argument.
    void erase(const Path<SrcEdges>& src)
    {
        m_map.erase(src);
    }
    void erase(Reference ref)
    {
        m_map.erase(ref);
    }
    void erase(ConstReference ref)
    {
        m_map.erase(ref);
    }

    // Returns a reference to the argument if it exists in this mapping,
    // otherwise it returns end().
    Reference find(const Path<SrcEdges>& src)
    {
        return m_map.find(src);
    }
    ConstReference find(const Path<SrcEdges>& src) const
    {
        return m_map.find(src);
    }

    // Bookkeeping: begin() refers to the "first" element (internally the
    // mappings are kept in sorted order according to the argument/key), and
    // end() refers to a one-past-the-last element. Thus, while begin() gives an
    // actual, valid element, end() instead yields an invalid element.
    // This fact is highly important and is used in find() and the like to
    // signify that no element was found.
    Reference begin() { return m_map.begin(); }
    ConstReference begin() const { return m_map.begin(); }
    Reference end() { return m_map.end(); }
    ConstReference end() const { return m_map.end(); }

    // When used on a StupidPathIsomorphism, this constructs an inverse
    // isomorphism, and returns that. The original one is preserved.
    StupidPathIsomorphism<DstEdges, SrcEdges> inverse() const
    {
        StupidPathIsomorphism<DstEdges, SrcEdges> inv;
        for (const auto& entry : m_map)
        {
            inv.insert(entry.second, entry.first);
        }
        return inv;
    }

    // Actual structure containing all of the mappings.
    InternalMap m_map;
};

// This is like StupidPathIsomorphism, except that it is much more sophisticated
// and internally stores the mappings in a vector (i.e. an array, so tightly
// packed in memory), and has optimised procedures for accessing and modifying
// this.
// Note that as a further restriction, as a result of the optimisation, this
// must know in advance what the possible paths to be mapped are (so it need to
// know the arguments in advance, but obviously not what they are actually
// mapped to).
template <U32 SrcEdges, U32 DstEdges>
struct VectorBasedPathIsomorphism
{
    struct PathMapping
    {
        union {Path<SrcEdges> src; Path<SrcEdges> first;}; // For name aliasing.
        union {Path<DstEdges> dst; Path<DstEdges> second;}; // For name aliasing.
        U32 compareCount = 2*SrcEdges + 2; // One only needs to compare .src[0, ..., compareCount-1] in m_mapping
                                           // to know exactly if this.src compares equal to another .src or not.

        void markUnallocated() { dst.vertices[0] = 0xffffffff; }
        bool allocated() const { return dst.vertices[0] != 0xffffffff; }
    };

    using Reference = PathMapping*;
    using ConstReference = const PathMapping*;

    // Functions for initialising with a set of possible input paths..
    void initPaths(const std::vector<Path<SrcEdges>>& v)
    {
        initPaths(v.data(), v.size());
    }

    void initPaths(const Path<SrcEdges>* paths, U32 count)
    {
        m_mapping.resize(count);
        for (U32 i = 0; i < count; ++i)
        {
            (m_mapping[i] = {paths[i], {}, 2*SrcEdges + 2}).markUnallocated();
        }
        finaliseInit();
    }
    void initialInsert(const Path<SrcEdges>& src, const Path<DstEdges>& dst)
    {
        m_mapping.push_back({src, dst, 2*SrcEdges + 2});
    }

    // This is called in initPaths() after adding all of the mappings, in order
    // to build various acceleration structures.
    void finaliseInit()
    {
        // Note: sorting using default comparison between the paths will sort
        // lexicographically, alternating between vertices and edges. That is
        // NOT the best way to compare here, since edges distinguish WAY better
        // than vertices. So we compare by edges first, *THEN* vertices.
        std::sort(m_mapping.begin(), m_mapping.end(),
                  [](const PathMapping& a, const PathMapping& b)
        {
            for (U32 i = 0; i < SrcEdges; ++i)
            {
                if (a.src.edges[i] < b.src.edges[i]) return true;
                if (a.src.edges[i] > b.src.edges[i]) return false;
            }
            for (U32 i = 0; i < SrcEdges+1; ++i)
            {
                if (a.src.vertices[i] < b.src.vertices[i]) return true;
                if (a.src.vertices[i] > b.src.vertices[i]) return false;
            }
            return false;
        });
        static_assert(offsetof(Path<SrcEdges>, edges) < offsetof(Path<SrcEdges>, vertices),
                      "Edges must come before vertices for optimisation.");
        static_assert(sizeof(Path<SrcEdges>) == sizeof(Path<SrcEdges>::vertices[0])*(2*SrcEdges+1),
                      "Path must contain tightly packed data.");
        static_assert(sizeof(Path<SrcEdges>::vertices[0]) == sizeof(U32),
                      "Path must be built from U32's or reinterpret_cast goes horribly wrong.");
        // But the type can be changed to anything else (e.g. U16) if it is also done so in this class.
        U32 maxLeadingEdge = 0;
        for (const auto& elem : m_mapping)
        {
            maxLeadingEdge = std::max(maxLeadingEdge, elem.src.edges[0]);
        }
        m_beginLookup.clear();
        m_beginLookup.resize(maxLeadingEdge+1, {0xffffffff, 0xffffffff});
        LookupRange* prevRange = nullptr;
        for (U32 i = 0; i < m_mapping.size(); ++i)
        {
            const U32* prevPath = i ? reinterpret_cast<const U32*>(&m_mapping[i-1].src) : nullptr;
            const U32* currPath = reinterpret_cast<const U32*>(&m_mapping[i].src);
            const U32* nextPath = i+1 < m_mapping.size() ? reinterpret_cast<const U32*>(&m_mapping[i+1].src) : nullptr;

            if (m_beginLookup[currPath[0]].begin == 0xffffffff)
            {
                if (prevRange) prevRange->end = i;
                m_beginLookup[currPath[0]].begin = i;
                prevRange = &m_beginLookup[currPath[0]];
            }

            for (U32 p = 0; p < 2*SrcEdges + 1; ++p)
            {
                bool differsFromPrev = !prevPath;
                bool differsFromNext = !nextPath;
                if (prevPath && prevPath[p] != currPath[p]) differsFromPrev = true;
                if (nextPath && nextPath[p] != currPath[p]) differsFromNext = true;

                if (differsFromPrev && differsFromNext)
                {
                    m_mapping[i].compareCount = p+1;
                }
            }
        }
    }

    Path<DstEdges> map(const Path<SrcEdges>& path)
    {
        return internalFind(path)->dst;
    }

    std::pair<Reference, bool> insert(const Path<SrcEdges>& src, const Path<DstEdges>& dst)
    {
        auto it = internalFind(src);
        if (it == end())
        {
            std::cerr << "Buggy data:\nsrc: " << src << "\ndst:\n" << dst << "\n";
            throw std::logic_error("Tried to insert something not in domain of path isomorphism.");
        }

        if (it->allocated()) return {it, false};
        it->dst = dst;
        return {it, true};
    }
    void erase(const Path<SrcEdges>& src)
    {
        erase(internalFind(src));
    }
    void erase(Reference ref)
    {
        ref->markUnallocated();
    }
    void erase(ConstReference ref)
    {
        const_cast<Reference>(ref)->markUnallocated();
    }

    Reference find(const Path<SrcEdges>& src)
    {
        auto ref = internalFind(src);
        if (ref != end() && !ref->allocated()) return end();
        return ref;
    }
    ConstReference find(const Path<SrcEdges>& src) const
    {
        ConstReference ref = internalFind(src);
        if (ref != end() && !ref->allocated()) return end();
        return ref;
    }

    Reference internalFind(const Path<SrcEdges>& src)
    {
        // Todo: Acceleration array + binary search can significantly cut down
        // on this. Acceleration array give O(1) for first edge, binary search
        // *may* be useful.
        auto [from, upto] = m_beginLookup[src.edges[0]];
        const U32* inputPath = reinterpret_cast<const U32*>(&src);
        for (U32 i = from; i < upto; ++i)
        {
            const U32* currPath = reinterpret_cast<const U32*>(&m_mapping[i].src);
            U32 compareCount = m_mapping[i].compareCount;
            for (U32 p = 0; p < compareCount; ++p)
            {
                if (currPath[p] != inputPath[p]) goto outerLoopBodyEnd;
            }
            return const_cast<Reference>(&m_mapping[i]);

        outerLoopBodyEnd:
            ;
        }
        return end();
    }

    Reference begin() { return m_mapping.data(); }
    ConstReference begin() const { return m_mapping.data(); }
    Reference end() { return m_mapping.data()+m_mapping.size(); }
    ConstReference end() const { return m_mapping.data()+m_mapping.size(); }

private:
    struct LookupRange
    {
        U32 begin;
        U32 end;
    };
    std::vector<PathMapping> m_mapping;
    std::vector<LookupRange> m_beginLookup;
};

// This is a slightly more sophisticated version of StupidPathIsomorphism,
// though the underlying principle is the same.
// As opposed to VectorBasedPathIsomorphism, this internally uses std::map,
// which is a tree data structure very likely stored non-contiguously, although
// in practise it is still quite fast, sometimes outperforming VectorBasedPath-
// Isomorphism, sometimes not.
template <U32 SrcEdges, U32 DstEdges>
struct MapBasedPathIsomorphism
{
    using InternalMap = std::map<Path<SrcEdges>, Path<DstEdges>>;
    using Reference = typename InternalMap::iterator;
    using ConstReference = typename InternalMap::const_iterator;

    // Unused in this implementation, but the functions are provided for this to
    // be a drop-in replacement.
    void initPaths(const std::vector<Path<SrcEdges>>&) {}
    void initPaths(const Path<SrcEdges>*, U32) {}

    bool hasEntry(const Path<SrcEdges>& path) const
    {
        return m_map.find(path) != m_map.end();
    }
    Path<DstEdges> map(const Path<SrcEdges>& path)
    {
        return m_map[path];
    }
    std::pair<Reference, bool> insert(const Path<SrcEdges>& src, const Path<DstEdges>& dst)
    {
        return m_map.insert({src, dst});
    }
    void erase(const Path<SrcEdges>& src)
    {
        m_map.erase(src);
    }
    void erase(Reference ref)
    {
        m_map.erase(ref);
    }
    void erase(ConstReference ref)
    {
        m_map.erase(ref);
    }

    Reference find(const Path<SrcEdges>& src)
    {
        return m_map.find(src);
    }
    ConstReference find(const Path<SrcEdges>& src) const
    {
        return m_map.find(src);
    }

    Reference begin() { return m_map.begin(); }
    ConstReference begin() const { return m_map.begin(); }
    Reference end() { return m_map.end(); }
    ConstReference end() const { return m_map.end(); }

    StupidPathIsomorphism<DstEdges, SrcEdges> inverse() const
    {
        StupidPathIsomorphism<DstEdges, SrcEdges> inv;
        for (const auto& entry : m_map)
        {
            inv.insert(entry.second, entry.first);
        }
        return inv;
    }

    InternalMap m_map;
};

////////////////////////////////////////////////////////////////////////////////
/// MAIN (AND VERY IMPORTANT) TYPEDEF!
/// This determines whether a PathIsomorphism should be either:
/// * MapBasedPathIsomorphism
/// * VectorBasedPathIsomorphism
/// Note that which one is fastest depends upon the actual problem in which they
/// are used, but MapBasedPathIsomorphism seems to be the fastest in most cases.
////////////////////////////////////////////////////////////////////////////////
template <U32 SrcEdges, U32 DstEdges>
using PathIsomorphism = MapBasedPathIsomorphism<SrcEdges, DstEdges>;


////////////////////////////////////////////////////////////////////////////////
/// The next few structures/classes and related functions are dedicated to a
/// special case of path isomorphisms, namely those between paths of length two.
////////////////////////////////////////////////////////////////////////////////
struct TwoEdgeInnerPath
{
    U32 e1;
    U32 e2;
    U32 middleVertex;
};

inline bool operator==(const TwoEdgeInnerPath& a, const TwoEdgeInnerPath& b)
{
    return a.e1 == b.e1 && a.e2 == b.e2 && a.middleVertex == b.middleVertex;
}

inline bool operator!=(const TwoEdgeInnerPath& a, const TwoEdgeInnerPath& b)
{
    return !(a == b);
}

inline bool operator<(const TwoEdgeInnerPath& a, const TwoEdgeInnerPath& b)
{
    if (a.e1 != b.e1) return a.e1 < b.e1;
    if (a.e2 != b.e2) return a.e2 < b.e2;
    return a.middleVertex < b.middleVertex;
}

struct DualIsomorphismEntry
{
    U32 startVertex;
    U32 endVertex;
    std::vector<TwoEdgeInnerPath> sourcePath;
    std::vector<TwoEdgeInnerPath> destPath;
    Permutation permutation;

    void finaliseInitialisation()
    {
        std::sort(sourcePath.begin(), sourcePath.end());
        std::sort(destPath.begin(), destPath.end());
    }

    TwoEdgeInnerPath map(TwoEdgeInnerPath src) const
    {
        for (U32 i = 0; i < sourcePath.size(); ++i)
        {
            if (sourcePath[i] == src) return destPath[permutation[i]];
        }
        throw std::logic_error("Invalid input path.");
    }
    bool tryMap(TwoEdgeInnerPath& path) const
    {
        auto it = std::lower_bound(sourcePath.begin(), sourcePath.end(), path);
        //if (it == sourcePath.end()) throw std::logic_error("Invalid input path in tryMap().");

        U32 i = std::distance(sourcePath.begin(), it);
        if (permutation[i] == Permutation::invalidEntry) return false;
        path = destPath[permutation[i]];
        return true;
    }
    TwoEdgeInnerPath inverseMap(TwoEdgeInnerPath dst) const
    {
        auto it = std::lower_bound(destPath.begin(), destPath.end(), dst);
        //if (it == sourcePath.end()) throw std::logic_error("Invalid input path in tryMap().");

        U32 at = std::distance(destPath.begin(), it);
        for (U32 i = 0; i < permutation.size(); ++i)
        {
            if (permutation[i] == at) return sourcePath[i];
        }
        throw std::logic_error("Bug: Should never get here.");
    }
};

struct DualPathIsomorphism
{
    void resize(U32 w, U32 h)
    {
        entries.resize(w*h);
        width = w;
    }
    DualIsomorphismEntry& operator()(U32 i, U32 j)
    {
        if (i >= width || j*width + i >= entries.size())
        {
            throw std::logic_error("Invalid index.");
        }
        return entries[j*width+i];
    }
    const DualIsomorphismEntry& operator()(U32 i, U32 j) const
    {
        if (i >= width || j*width + i >= entries.size())
        {
            throw std::logic_error("Invalid index.");
        }
        return entries[j*width+i];
    }

    DualPathIsomorphism inverse() const;

    Path<2> operator()(Path<2> p) const
    {
        TwoEdgeInnerPath innerPath;
        innerPath.e1 = p.edges[0];
        innerPath.e2 = p.edges[1];
        innerPath.middleVertex = p.vertices[1];
        innerPath = (*this)(p.vertices[0], p.vertices[2]).map(innerPath);
        p.edges[0] = innerPath.e1;
        p.edges[1] = innerPath.e2;
        p.vertices[1] = innerPath.middleVertex;
        return p;
    }
    bool tryMap(Path<2>& p) const
    {
        TwoEdgeInnerPath innerPath;
        innerPath.e1 = p.edges[0];
        innerPath.e2 = p.edges[1];
        innerPath.middleVertex = p.vertices[1];
        if (!(*this)(p.vertices[0], p.vertices[2]).tryMap(innerPath)) return false;
        p.edges[0] = innerPath.e1;
        p.edges[1] = innerPath.e2;
        p.vertices[1] = innerPath.middleVertex;
        return true;
    }

    std::vector<DualIsomorphismEntry> entries;
    U32 width;
};

////////////////////////////////////////////////////////////////////////////////
/// The remainder of this file contains data structures enabling backtracking
/// algorithms, and in particular, partially constructing various isomorphism
/// and later undoing such partial constructions.
/// This is critical in order to greatly cut down the number of possible
/// isomorphisms to search through.
////////////////////////////////////////////////////////////////////////////////

template <U32 EdgeCount>
using MultistepPathCollection = std::vector<Path<EdgeCount>>;

template <U32 M>
struct MultistepPathSearchData
{
    MultistepPathCollection<M> paths;
    std::vector<U32> free; // Indices in “paths” in which we do not know the
                           // image.
    std::vector<U32> used; // These paths are marked as being known to a partial
                           // isomorphism.
};

template <U32 M>
struct BacktrackingSearchData
{
    MultistepPathSearchData<M+1> ms;
    MultistepPathSearchData<M+2> psiMS;

    DualPathIsomorphism phi;

    PathIsomorphism<M+1, M+1> multistepPhi;
    PathIsomorphism<M+1, M+1> multistepPhiInverse;
    PathIsomorphism<2, M> psi;
    PathIsomorphism<M, 2> psiInverse;

    std::vector<typename PathIsomorphism<M+1, M+1>::Reference> multistepPhiUsed;
    std::vector<typename PathIsomorphism<M+1, M+1>::Reference> multistepPhiInverseUsed;
    std::vector<typename PathIsomorphism<2, M>::Reference> psiUsed;
    std::vector<typename PathIsomorphism<M, 2>::Reference> psiInverseUsed;
};

struct PhiLocation
{
    U32 mspathUsedBegin;
    U32 msPhiUsedBegin;
    U32 msPhiInverseUsedBegin;
};

struct PsiLocation
{
    U32 mspathUsedBegin;
    U32 msPsiUsedBegin;
    U32 msPsiInverseUsedBegin;
};

struct PartialFillLocation
{
    PhiLocation phiR;
    PhiLocation phiS;
    PsiLocation psiR;
    PsiLocation psiS;
};

struct CompatibleIsomorphismsFound{};

#endif // TYPES_HPP_INCLUDED
