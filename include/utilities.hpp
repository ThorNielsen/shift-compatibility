#ifndef UTILITIES_HPP_INCLUDED
#define UTILITIES_HPP_INCLUDED

#include "types.hpp"

// Exponentiation via repeated squaring.
template <size_t Dim, typename Prec>
Matrix<Dim, Dim, Prec> operator^(Matrix<Dim, Dim, Prec> mat, Prec exponent)
{
    Matrix<Dim, Dim, Prec> res(Prec(1));
    while (exponent)
    {
        if (exponent & 1) res *= mat;
        mat *= mat;
        exponent >>= 1;
    }
    return res;
}

template<size_t Adim, size_t Bdim, typename Prec>
bool areMatrixConditionsSatisfied(Matrix<Adim, Adim, Prec> A,
                                  Matrix<Bdim, Bdim, Prec> B,
                                  Matrix<Adim, Bdim, Prec> R,
                                  Matrix<Bdim, Adim, Prec> S,
                                  Prec m)
{
    return A*R == R*B && B*S == S*A && (A^m) == R*S && (B^m) == S*R;
}

// Since each entry in some matrices gives the number of paths from one vertex
// to another in the graph, we need to use consistent enumeration of those
// paths. This is solved by giving each path a numeric id starting from 0, and
// we do this by creating a new matrix whose entries contains the starting
// indices of each path.
// As an example, if we have the matrix
// 2 3
// 1 4
// then we construct a matrix like the following:
// 0 2
// 5 6
// which indicates that we have the following path ids:
// [0, 1] [2, 3, 4]
// [5]    [6, 7, 8, 9]
// I.e. by using this start index matrix along with the matrix giving the number
// of paths / edges, then we know exactly which paths runs between which
// vertices of the graph.
template <size_t Rows, size_t Cols>
Matrix<Rows, Cols, U32> computeEdgeStartIndices(Matrix<Rows, Cols, U32> m)
{
    U32 carry = 0;
    for (U32 i = 0; i < Rows; ++i)
    {
        for (U32 j = 0; j < Cols; ++j)
        {
            U32 val = m(i, j);
            if (m(i, j)) m(i, j) = carry;
            carry += val;
        }
    }
    return m;
}

inline DualPathIsomorphism DualPathIsomorphism::inverse() const
{
    DualPathIsomorphism iso = (*this);
    for (auto& entry : iso.entries)
    {
        std::swap(entry.startVertex, entry.endVertex);
        std::swap(entry.sourcePath, entry.destPath);
        entry.permutation = entry.permutation.inverse();
    }
    return iso;
}

template <size_t Adim, size_t Bdim>
DualPathIsomorphism initialiseDualIsomorphism(Matrix<Adim, Adim, U32> A,
                                              Matrix<Adim, Bdim, U32> R,
                                              Matrix<Bdim, Bdim, U32> B)
{
    DualPathIsomorphism isomorphism;
    isomorphism.resize(Adim, Bdim);

    auto Aaux = computeEdgeStartIndices(A);
    auto Raux = computeEdgeStartIndices(R);
    auto Baux = computeEdgeStartIndices(B);

    for (U32 i = 0; i < Adim; ++i)
    {
        for (U32 j = 0; j < Bdim; ++j)
        {
            auto& currEntry = isomorphism(i, j);
            currEntry.startVertex = i;
            currEntry.endVertex = j;

            for (U32 k = 0; k < Adim; ++k)
            {
                for (U32 x = 0; x < A(i, k); ++x)
                {
                    for (U32 y = 0; y < R(k, j); ++y)
                    {
                        TwoEdgeInnerPath path;
                        path.middleVertex = k;
                        path.e1 = Aaux(i, k) + x;
                        path.e2 = Raux(k, j) + y;
                        currEntry.sourcePath.push_back(path);
                    }
                }
            }

            for (U32 k = 0; k < Bdim; ++k)
            {
                for (U32 x = 0; x < R(i, k); ++x)
                {
                    for (U32 y = 0; y < B(k, j); ++y)
                    {
                        TwoEdgeInnerPath path;
                        path.middleVertex = k;
                        path.e1 = Raux(i, k) + x;
                        path.e2 = Baux(k, j) + y;
                        currEntry.destPath.push_back(path);
                    }
                }
            }

            if (currEntry.sourcePath.size() != currEntry.destPath.size())
            {
                throw std::logic_error("Source and destination path counts "
                                       "differ, indicating that we do not in "
                                       "fact have an isomorphism!");
            }
            currEntry.permutation.resize(currEntry.sourcePath.size());
            currEntry.finaliseInitialisation();
        }
    }
    return isomorphism;
}


// Finds all multistep paths of length EdgeCount, where the first "index" of
// of them start in E_A, then there is one in E_R and finally the rest are in
// E_S.
template <U32 EdgeCount, size_t DimA, size_t DimR, size_t DimB, size_t DimS>
void getAllMultistepPaths(const Matrix<DimA, DimR, U32>& A,
                          const Matrix<DimA, DimR, U32>& Aaux, U32 aCount,
                          const Matrix<DimR, DimB, U32>& R,
                          const Matrix<DimR, DimB, U32>& Raux,
                          const Matrix<DimB, DimS, U32>& B,
                          const Matrix<DimB, DimS, U32>& Baux,
                          U32 index,
                          Path<EdgeCount> path,
                          std::vector<Path<EdgeCount>>& appendTo)
{
    if (index == EdgeCount)
    {
        appendTo.push_back(path);
        return;
    }
    U32 currVert = path.vertices[index];
    if (index < aCount)
    {
        for (U32 nextVert = 0; nextVert < DimR; ++nextVert)
        {
            path.vertices[index+1] = nextVert;
            for (U32 x = 0; x < A(currVert, nextVert); ++x)
            {
                path.edges[index] = Aaux(currVert, nextVert) + x;

                getAllMultistepPaths(A, Aaux, aCount, R, Raux, B, Baux, index+1, path, appendTo);
            }
        }
    }
    else if (index == aCount)
    {
        for (U32 nextVert = 0; nextVert < DimB; ++nextVert)
        {
            path.vertices[index+1] = nextVert;
            for (U32 x = 0; x < R(currVert, nextVert); ++x)
            {
                path.edges[index] = Raux(currVert, nextVert) + x;

                getAllMultistepPaths(A, Aaux, aCount, R, Raux, B, Baux, index+1, path, appendTo);
            }
        }
    }
    else
    {
        for (U32 nextVert = 0; nextVert < DimS; ++nextVert)
        {
            path.vertices[index+1] = nextVert;
            for (U32 x = 0; x < B(currVert, nextVert); ++x)
            {
                path.edges[index] = Baux(currVert, nextVert) + x;

                getAllMultistepPaths(A, Aaux, aCount, R, Raux, B, Baux, index+1, path, appendTo);
            }
        }
    }
}


// Enumerates all paths aCount times in A, 1 time in R and the remaining ones in B.
template <U32 EdgeCount, size_t DimA, size_t DimR, size_t DimB, size_t DimS>
std::vector<Path<EdgeCount>>
getAllMultistepPaths(Matrix<DimA, DimR, U32> A,
                     Matrix<DimR, DimB, U32> R,
                     Matrix<DimB, DimS, U32> B,
                     U32 aCount)
{
    auto Aaux = computeEdgeStartIndices(A);
    auto Raux = computeEdgeStartIndices(R);
    auto Baux = computeEdgeStartIndices(B);
    std::vector<Path<EdgeCount>> paths;
    Path<EdgeCount> p;
    // Initialise to squelch maybe-uninitialised warnings (it is not a problem
    // that this is not initialised -- it will be completely overwritten).
    for (U32 i = 0; i < EdgeCount; ++i)
    {
        p.edges[i] = ~0u;
        p.vertices[i+1] = ~0u;
    }

    for (U32 start = 0; start < DimA; ++start)
    {
        p.vertices[0] = start;
        getAllMultistepPaths(A, Aaux, aCount, R, Raux, B, Baux, 0, p, paths);
    }
    return paths;
}

// Converts a two-step isomorphism into the multistep version. The given paths
// are the ones that the multistep version can be applied on, so e.g. all the
// elements of E_A×E_A×...×E_A×E_R.
template <U32 M>
StupidPathIsomorphism<M, M> getMultistepIsomorphism(const DualPathIsomorphism& srcIso,
                                                    const Path<M>* paths,
                                                    U32 pathCount)
{
    StupidPathIsomorphism<M, M> iso;

    for (U32 i = 0; i < pathCount; ++i)
    {
        Path<M> curr = paths[i];
        for (U32 chop = 0; chop <= M - 2; ++chop)
        {
            U32 begin = M-2-chop;
            curr.replace(srcIso(curr.template subpath<2>(begin)), begin);
        }
        iso.insert(paths[i], curr);
    }
    return iso;
}

/// WARNING: SLOW! Computes inverses of isomorphisms. There *should* be a much
///          efficient way to extract \psi than this, but it is not really clear
///          how to do so at this moment.
// For psi_A, the input edges should be R×S×(A^M)
// Note here that phi_r is the multistep isomorphism, i.e.
// phi_r : (A^M)×R → S×(B^M)
// Similarly,
// phi_s : (B^M)×S → R×(A^M)
template <U32 M>
StupidPathIsomorphism<2, M> extractPsi(const StupidPathIsomorphism<M+1, M+1>& phi_R_ms,
                                       const StupidPathIsomorphism<M+1, M+1>& phi_S_ms,
                                       const Path<M+2>* paths,
                                       U32 pathCount)
{
    auto phi_R_inv = phi_R_ms.inverse();
    auto phi_S_inv = phi_S_ms.inverse();

    StupidPathIsomorphism<2, M> psi_A;

    for (U32 i = 0; i < pathCount; ++i)
    {
        Path<M+2> currPath = paths[i];
        currPath.replace(phi_S_inv.map(currPath.template subpath<M+1>(1)), 1);
        currPath.replace(phi_R_inv.map(currPath.template subpath<M+1>(0)), 0);
        psi_A.insert(paths[i].template subpath<2>(0), currPath.template subpath<M>(0));
    }
    return psi_A;
}

/// WARNING: SLOW! Computes inverses of isomorphisms.
template <U32 M>
bool isCompatible(const StupidPathIsomorphism<M+1, M+1>& phi_R_ms,
                  const StupidPathIsomorphism<M+1, M+1>& phi_S_ms,
                  StupidPathIsomorphism<2, M>& psi_A,
                  StupidPathIsomorphism<2, M>& psi_B)
{
    auto psi_A_inv = psi_A.inverse();
    auto psi_B_inv = psi_B.inverse();

    //StatWriter sw{0, phi_R_ms.size() + phi_S_ms.size()};

    //std::cout << phi_R_ms.size() << "\n";
    //std::cin.get();
    try
    {
        for (const auto& entry : phi_S_ms)
        {
            auto path0 = psi_B_inv.map(entry.first.template subpath<M>(0))
                       + entry.first.template subpath<1>(M);
            auto path1 = path0.template subpath<1>(0)
                       + psi_A.map(path0.template subpath<2>(1));
            if (path1 != entry.second) return false;
            //++sw.count;
        }
        for (const auto& entry : phi_R_ms)
        {
            auto path0 = psi_A_inv.map(entry.first.template subpath<M>(0))
                       + entry.first.template subpath<1>(M);
            auto path1 = path0.template subpath<1>(0)
                       + psi_B.map(path0.template subpath<2>(1));
            if (path1 != entry.second) return false;
            //++sw.count;
        }
    }
    catch (const NoCommonVertexError&)
    {
        // This just means that we actually couldn't extract a \psi-map
        // correctly. Ideally, this should be caught *much* earlier.

        // It actually appears to indicate a bug in the extraction of the \psi-
        // maps so we actually shouldn't catch this. I'm re-throwing instead of
        // just removing the catch in case we might hit this in a normal
        // situation.
        //throw;
        return false;
    }

    return true;
}

// This determines compatibility of two two-step isomorphisms, by computing the
// multistep versions, extracting psi maps and then checking compatibility.
// While this was originally used for the exhaustive search, it is now used as
// an extra sanity check for our backtracking search -- if something appears to
// be compatible, then we can check that (slightly more convoluted) logic by
// using this function to verify that we have not made a mistake somewhere in
// the backtracking search.
template <U32 M>
bool arePhiMapsCompatible(const DualPathIsomorphism& phi_R,
                          const DualPathIsomorphism& phi_S,
                          const MultistepPathCollection<M+1>& multistepPathsA,
                          const MultistepPathCollection<M+1>& multistepPathsB,
                          const MultistepPathCollection<M+2>& psiMultistepPathsA,
                          const MultistepPathCollection<M+2>& psiMultistepPathsB)
{
    auto phi_R_ms = getMultistepIsomorphism(phi_R, multistepPathsA.data(), multistepPathsA.size());
    auto phi_S_ms = getMultistepIsomorphism(phi_S, multistepPathsB.data(), multistepPathsB.size());

    auto psi_A = extractPsi<M>(phi_R_ms, phi_S_ms, psiMultistepPathsA.data(), psiMultistepPathsA.size());
    auto psi_B = extractPsi<M>(phi_S_ms, phi_R_ms, psiMultistepPathsB.data(), psiMultistepPathsB.size());

    return isCompatible(phi_R_ms, phi_S_ms, psi_A, psi_B);
}

#endif // UTILITIES_HPP_INCLUDED
