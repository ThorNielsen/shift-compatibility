use_cpp17()

set(INCDIR ${PROJECT_SOURCE_DIR}/include)
set(SRCDIR ${PROJECT_SOURCE_DIR}/src)

set(HEADERS
    ${INCDIR}/formatting.hpp
    ${INCDIR}/searches.hpp
    ${INCDIR}/types.hpp
    ${INCDIR}/utilities.hpp
)

set(SOURCES
    ${HEADERS}
    ${SRCDIR}/main.cpp
)

add_executable(shift-compatibility ${SOURCES})
set_target_properties(shift-compatibility PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(shift-compatibility pthread)
install(TARGETS shift-compatibility RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
