#define KRAKEN_ALLOW_DEGENERATE_MATRICES
#include <kraken/globaltypes.hpp>
#include <kraken/math/matrix.hpp>
#include <atomic>
#include <fstream>
#include <iostream>
#include <list>
#include <numeric>
#include <map>
#include <mutex>
#include <thread>

#include "types.hpp"
#include "utilities.hpp"
#include "formatting.hpp"

#include <kraken/utility/timer.hpp>

using namespace kraken::math;

// Whether to use coloured output. This is recommended to be enabled unless one
// uses a terminal without support for this, or if the output is to be written
// to a file, in which case this will likely clutter the incompatible terminal
// or the file with random-looking characters.
std::atomic<bool> g_useColourCodes;

// Whether to find all isomorphisms or to stop after one was found. If there are
// no isomorphisms, this obviously has no effect, since all possibilities are
// exhausted in any case.
std::atomic<bool> g_findAllIsomorphisms;

// This is a trick to work around a certain complication of the data structures.
// The problem is that the vector-based data structure is tightly packed, so any
// deleted element is deleted by just writing a "deleted" flag somewhere in the
// element -- and that is tested by the .allocated()-function.
// Thus we need to test that.
template <typename Reference>
bool isReferenceValid(const Reference& ref)
{
    return ref.allocated();
}

// But if we are using the map-based data structures, then there is no such
// complication, and any reference is always valid.
template <U32 SrcEdges, U32 DstEdges>
bool isReferenceValid(const std::pair<const Path<SrcEdges>, Path<DstEdges>>&)
{
    return true;
}

// Test whether two partially constructed isomorphisms can be compatible or not.
// This is highly important in order to quickly eliminate branches of the search
// tree -- if we at some point can determine that there is no way to make the
// two isomorphisms compatible, then we just backtrack.
template <U32 M>
bool arePossiblyCompatibleInnerLoop(BacktrackingSearchData<M>& R,
                                    BacktrackingSearchData<M>& S)
{
    for (const auto& entry : S.multistepPhi)
    {
        if (!isReferenceValid(entry)) continue;
        auto spsi = S.psiInverse.find(entry.first.template subpath<M>(0));
        if (spsi == S.psiInverse.end()) continue;

        auto path0 = spsi->second
                   + entry.first.template subpath<1>(M);

        auto rpsi = R.psi.find(path0.template subpath<2>(1));
        if (rpsi == R.psi.end()) continue;

        auto path1 = path0.template subpath<1>(0)
                   + rpsi->second;
        if (path1 != entry.second) return false;
        //++sw.count;
    }
    return true;
}

// Determine if the partially (or possibly fully) filled R and S things can lead
// to a compatible isomorphism. If both are fully filled, this MUST act as
// isCompatible()...
template <U32 M>
bool arePossiblyCompatible(BacktrackingSearchData<M>& R,
                           BacktrackingSearchData<M>& S)
{
    try
    {
        return arePossiblyCompatibleInnerLoop(R, S)
               && arePossiblyCompatibleInnerLoop(S, R);
    }
    catch (const NoCommonVertexError&)
    {
        // This just means that we actually couldn't extract a \psi-map
        // correctly. Ideally, this should be caught *much* earlier.

        // It actually appears to indicate a bug in the extraction of the \psi-
        // maps so we actually shouldn't catch this. I'm re-throwing instead of
        // just removing the catch in case we might hit this in a normal
        // situation.
        //throw;
        return false;
    }

    return true;
}

// Returns true if the path was successfully mapped -- if not, then path may be
// partially modified, and should be discarded.
template <U32 M>
bool fillPath(Path<M>& path, const DualPathIsomorphism& dpi)
{
    for (U32 chop = 0; chop <= M - 2; ++chop)
    {
        U32 begin = M-2-chop;
        Path<2> replacement = path.template subpath<2>(begin);
        if (!dpi.tryMap(replacement)) return false;

        path.replace(replacement, begin);
    }
    return true;
}

// Tries to insert a mapping into phi, but returns false if that is not
// possible, i.e. if that would lead to non-compatible isomorphisms in some way.
template <U32 M>
bool tryInsertPhi(const Path<M+1>& inputPath,
                  const Path<M+1>& dstPath,
                  BacktrackingSearchData<M>& phi)
{
    // Psi-insertion
    {
        auto [it, insertResult] = phi.multistepPhi.insert(inputPath, dstPath);

        if (!insertResult)
        {
            if (it->second != dstPath) return false;
        }
        else
        {
            phi.multistepPhiUsed.push_back(it);
        }
    }
    // Psi-inverse-insertion (todo: think about whether this can even fail if
    // the above does not fail).
    {
        auto [it, insertResult] = phi.multistepPhiInverse.insert(dstPath, inputPath);
        if (!insertResult)
        {
            if (it->second != inputPath) return false;
        }
        else
        {
            phi.multistepPhiInverseUsed.push_back(it);
        }
    }
    return true;
}

// Updates R and S maximally, and returns (by the reference phiLocOut) some
// value/location which can be given to partialUnfill to revert this operation.
// The return value determines whether it was possible to fill phi without
// necessarily getting a non-compatible isomorphism.
template <U32 M>
bool partialFillPhi(BacktrackingSearchData<M>& phi,
                    PhiLocation& phiLocOut)
{
    phiLocOut.mspathUsedBegin = phi.ms.used.size();
    phiLocOut.msPhiUsedBegin = phi.multistepPhiUsed.size();
    phiLocOut.msPhiInverseUsedBegin = phi.multistepPhiInverseUsed.size();

    for (U32 i = 0; i < phi.ms.free.size();)
    {
        auto curr = phi.ms.free[i];
        Path<M+1> p = phi.ms.paths[curr];
        if (fillPath(p, phi.phi))
        {
            if (!tryInsertPhi(phi.ms.paths[curr], p, phi))
            {
                return false;
            }
            std::swap(phi.ms.free[i], phi.ms.free.back());
            phi.ms.free.pop_back();
            phi.ms.used.push_back(curr);
        }
        else
        {
            ++i;
        }
    }
    return true;
}

// Similar to tryInserPhi, just with psi instead.
template <U32 M>
bool tryInsertPsi(const Path<2>& inputPath,
                  const Path<M>& dstPath,
                  BacktrackingSearchData<M>& R)
{
    // Psi-insertion
    {
        auto [it, insertResult] = R.psi.insert(inputPath, dstPath);
        if (!insertResult)
        {
            if (it->second != dstPath) return false;
        }
        else
        {
            R.psiUsed.push_back(it);
        }
    }
    // Psi-inverse-insertion (todo: think about whether this can even fail if
    // the above does not fail).
    {
        auto [it, insertResult] = R.psiInverse.insert(dstPath, inputPath);
        if (!insertResult)
        {
            if (it->second != inputPath) return false;
        }
        else
        {
            R.psiInverseUsed.push_back(it);
        }
    }
    return true;
}

// This fills psi_R, but can symmetrically be used for psi_S by just swapping
// the arguments.
// The return value determines whether psi becomes well-defined or not -- if it
// is not well-defined
template <U32 M>
bool partialFillPsi(BacktrackingSearchData<M>& R,
                    BacktrackingSearchData<M>& S,
                    PsiLocation& psiloc)
{
    psiloc.mspathUsedBegin = R.psiMS.used.size();
    psiloc.msPsiUsedBegin = R.psiUsed.size();
    psiloc.msPsiInverseUsedBegin = R.psiInverseUsed.size();
    for (U32 i = 0; i < R.psiMS.free.size();)
    {
        auto curr = R.psiMS.free[i];
        auto inputPath = R.psiMS.paths[curr].template subpath<2>(0);
        Path<M+2> currPath = R.psiMS.paths[curr];

        auto sIt = S.multistepPhiInverse.find(currPath.template subpath<M+1>(1));
        if (sIt == S.multistepPhiInverse.end())
        {
            ++i;
            continue;
        }
        currPath.replace(sIt->second, 1);

        auto rIt = R.multistepPhiInverse.find(currPath.template subpath<M+1>(0));
        if (rIt == R.multistepPhiInverse.end())
        {
            ++i;
            continue;
        }
        currPath.replace(rIt->second, 0);

        if (!tryInsertPsi(inputPath, currPath.template subpath<M>(0), R))
        {
            return false;
        }

        std::swap(R.psiMS.free[i], R.psiMS.free.back());
        R.psiMS.free.pop_back();
        R.psiMS.used.push_back(curr);
    }
    return true;
}

// Partially fills out the isomorphisms using the permutations that have been
// fixed so far.
template <U32 M>
PartialFillLocation partialFill(BacktrackingSearchData<M>& R,
                                BacktrackingSearchData<M>& S,
                                bool& success)
{
    success = true;
    PartialFillLocation pfl;
    pfl.phiR.mspathUsedBegin = R.ms.used.size();
    pfl.phiR.msPhiUsedBegin = R.multistepPhiUsed.size();
    pfl.phiR.msPhiInverseUsedBegin = R.multistepPhiInverseUsed.size();
    pfl.phiS.mspathUsedBegin = S.ms.used.size();
    pfl.phiS.msPhiUsedBegin = S.multistepPhiUsed.size();
    pfl.phiS.msPhiInverseUsedBegin = S.multistepPhiInverseUsed.size();

    pfl.psiR.mspathUsedBegin = R.psiMS.used.size();
    pfl.psiR.msPsiUsedBegin = R.psiUsed.size();
    pfl.psiR.msPsiInverseUsedBegin = R.psiInverseUsed.size();
    pfl.psiS.mspathUsedBegin = S.psiMS.used.size();
    pfl.psiS.msPsiUsedBegin = S.psiUsed.size();
    pfl.psiS.msPsiInverseUsedBegin = S.psiInverseUsed.size();

    if (success && !partialFillPhi(R, pfl.phiR)) success = false;
    if (success && !partialFillPhi(S, pfl.phiS)) success = false;

    if (success && !partialFillPsi(R, S, pfl.psiR)) success = false;
    if (success && !partialFillPsi(S, R, pfl.psiS)) success = false;
    return pfl;
}


// Undoes partialFillPhi.
template <U32 M>
void partialUnfillPhi(BacktrackingSearchData<M>& phi,
                      PhiLocation loc)
{
    while (phi.ms.used.size() > loc.mspathUsedBegin)
    {
        phi.ms.free.push_back(phi.ms.used.back());
        phi.ms.used.pop_back();
    }
    while (phi.multistepPhiUsed.size() > loc.msPhiUsedBegin)
    {
        phi.multistepPhi.erase(phi.multistepPhiUsed.back());
        phi.multistepPhiUsed.pop_back();
    }
    while (phi.multistepPhiInverseUsed.size() > loc.msPhiInverseUsedBegin)
    {
        phi.multistepPhiInverse.erase(phi.multistepPhiInverseUsed.back());
        phi.multistepPhiInverseUsed.pop_back();
    }
}

// Undoes partialFillPsi.
template <U32 M>
void partialUnfillPsi(BacktrackingSearchData<M>& R,
                      PsiLocation loc)
{
    while (R.psiMS.used.size() > loc.mspathUsedBegin)
    {
        R.psiMS.free.push_back(R.psiMS.used.back());
        R.psiMS.used.pop_back();
    }
    while (R.psiUsed.size() > loc.msPsiUsedBegin)
    {
        R.psi.erase(R.psiUsed.back());
        R.psiUsed.pop_back();
    }
    while (R.psiInverseUsed.size() > loc.msPsiInverseUsedBegin)
    {
        R.psiInverse.erase(R.psiInverseUsed.back());
        R.psiInverseUsed.pop_back();
    }
}

// Undoes partialFillPsi -- critical for backtracking.
template <U32 M>
void partialUnfill(BacktrackingSearchData<M>& R,
                   BacktrackingSearchData<M>& S,
                   PartialFillLocation loc)
{
    partialUnfillPsi(S, loc.psiS);
    partialUnfillPsi(R, loc.psiR);
    partialUnfillPhi(S, loc.phiS);
    partialUnfillPhi(R, loc.phiR);
}

// This wraps the fill-unfill functions in a class where the constructor
// performs the filling, one can then use the partially filled isomorphisms,
// and the destructor then undoes the operations yielding the original
// backtracking data.
template <U32 M>
class PartialFiller
{
public:
    PartialFiller(BacktrackingSearchData<M>& R,
                  BacktrackingSearchData<M>& S,
                  PartialFillLocation* out = nullptr,
                  bool* successOut = nullptr)
        : m_R{R}, m_S{S}
    {
        m_loc = partialFill(R, S, m_success);
        if (out) *out = m_loc;
        if (successOut) *successOut = m_success;
    }
    ~PartialFiller()
    {
        partialUnfill(m_R, m_S, m_loc);
    }

private:
    BacktrackingSearchData<M>& m_R;
    BacktrackingSearchData<M>& m_S;
    PartialFillLocation m_loc;
    bool m_success;
};

// Non-owning pointers to multistep paths to enable sharing between threads
// without unnecessary copying.
template<U32 M>
struct SearchData
{
    const MultistepPathCollection<M+1>* multistepPathsA;
    const MultistepPathCollection<M+1>* multistepPathsB;
    const MultistepPathCollection<M+2>* psiMultistepPathsA;
    const MultistepPathCollection<M+2>* psiMultistepPathsB;
};

// Global variables. Made atomic to enable updating on different threads without
// any synchronisation issues.
std::atomic<U64> g_count;
std::atomic<U64> g_maxCount;

std::atomic<S32> g_currentThreadCount; // Active threads. Some may be sleeping or waiting.
std::atomic<S32> g_maxActiveThreadCount;

U32 g_entryCount;

template <U32 M>
struct ThreadRunInfo
{
    BacktrackingSearchData<M> R;
    BacktrackingSearchData<M> S;
    std::vector<DualIsomorphismEntry*> entries;
    std::vector<U16> permChoices;
    std::thread thread;
};

// Computes the factorial recursively.
U64 fac(U64 n)
{
    return n ? n*fac(n-1) : 1;
}

// This function highly speeds up later computation. The idea is that both R and
// S have certain permutations that effectively need to brute-force, except that
// after having chosen a few permutations (maybe partially, if they are large),
// then at that point we might be able to reject the current choices as leading
// to non-compatible isomorphisms.
// What we do here is to collect pointers to those permuations, merge them to a
// single array of pointers and sort that based on permutation size. The effect
// of this is that we fix smaller permutations before larger ones, thus in many
// cases giving that we never need to compute a large permutation before
// rejecting the current option.´
template <U32 M>
std::vector<DualIsomorphismEntry*> getSortedEntries(BacktrackingSearchData<M>& R,
                                                    BacktrackingSearchData<M>& S,
                                                    bool clearPermutation)
{
    std::vector<DualIsomorphismEntry*> entries;
    auto commonSize = std::min(R.phi.entries.size(), S.phi.entries.size());
    for (U32 i = 0; i < commonSize; ++i)
    {
        if (R.phi.entries[i].permutation.size()) entries.push_back(&R.phi.entries[i]);
        if (S.phi.entries[i].permutation.size()) entries.push_back(&S.phi.entries[i]);
    }

    for (U32 i = commonSize; i < R.phi.entries.size(); ++i)
    {
        if (R.phi.entries[i].permutation.size()) entries.push_back(&R.phi.entries[i]);
    }
    for (U32 i = commonSize; i < S.phi.entries.size(); ++i)
    {
        if (S.phi.entries[i].permutation.size()) entries.push_back(&S.phi.entries[i]);
    }
    if (clearPermutation)
    {
        for (auto entry : entries)
        {
            g_maxCount = g_maxCount * fac(entry->permutation.size());
            for (auto& p : entry->permutation.order)
            {
                p = Permutation::invalidEntry;
            }
        }
    }

    std::stable_sort(entries.begin(), entries.end(),
                     [](DualIsomorphismEntry* a, DualIsomorphismEntry* b)
    {
        if (a->permutation.size() == b->permutation.size())
        {
            return a < b;
        }
        return a->permutation.size() < b->permutation.size();
    });
    return entries;
}


// This does exactly as the original exhaustive search, instead that in EVERY
// step, it uses PartialFiller and only checks differences / newly added data
// for an early out.
/// TODO:
/// [X] Implement partial permutations.
/// [X] Add automatic multi-threading.
template <U32 M>
void cleverExhaustiveSearch(BacktrackingSearchData<M>& R,
                            BacktrackingSearchData<M>& S,
                            DualIsomorphismEntry** entryBegin,
                            DualIsomorphismEntry** entries,
                            U32 entryCount,
                            std::vector<U16>* partialPermutationEntriesLeft)
{
    if (!entryCount)
    {
        if (arePossiblyCompatible(R, S))
        {
            if (!arePhiMapsCompatible<M>(R.phi, S.phi,
                                         R.ms.paths, S.ms.paths,
                                         R.psiMS.paths, S.psiMS.paths))
            {
                // If you read this, then observe that arePhiMapsCompatible is
                // very well tested, and the error is likely in some partial
                // computation such that arePossiblyCompatible does NOT get
                // full isomorphisms.
                // So the conclusion is: Check that arePossiblyCompatible does
                // indeed get fully evaluated isomorphisms before doing this.
                throw std::logic_error("BUG: arePossiblyCompatible does NOT "
                                       "agree with arePhiMapsCompatible!");
            }
            if (g_findAllIsomorphisms)
            {
                std::cout << "Compatible found:\nphi_R:\n" << R.phi << "\nphi_S:\n" << S.phi << "\n\n";
            }
            else
            {
                std::cout << "Compatible found:\nphi_R:\n" << R.phi << "\nphi_S:\n" << S.phi << "\n\n";
                std::ofstream results("results.txt");
                g_useColourCodes = false;
                results << "Compatible found:\nphi_R:\n" << R.phi << "\nphi_S:\n" << S.phi << "\n\n";
                results.close();
                results.open("results_coloured.txt");
                g_useColourCodes = true;
                results << "Compatible found:\nphi_R:\n" << R.phi << "\nphi_S:\n" << S.phi << "\n\n";
                results.close();

                if (g_maxActiveThreadCount > 1)
                {
                    std::cout << "Currently in multi-threading mode; forcibly "
                                 "ending program while other threads might "
                                 "still be running."
                              << std::endl;
                    std::exit(0);
                }
                throw CompatibleIsomorphismsFound{};
            }
        }
    }
    else
    {
        std::vector<U16> permStorage;

        U32 permInsertLoc = 0;
        if (partialPermutationEntriesLeft)
        {
            permInsertLoc = entries[0]->permutation.size()
                            - partialPermutationEntriesLeft->size();
        }
        else
        {
            permStorage.resize(entries[0]->permutation.size());
            std::iota(permStorage.begin(), permStorage.end(), 0);
            std::random_shuffle(permStorage.begin(), permStorage.end());
        }
        std::vector<U16>& permChoices = partialPermutationEntriesLeft
                                        ? *partialPermutationEntriesLeft
                                        : permStorage;

        std::list<ThreadRunInfo<M>> threads;

        U32 choiceCount = permChoices.size();
        for (U32 i = 0; i < choiceCount; ++i)
        {
            ++g_count;
            if (g_count % 100000 == 0)
            {
                U64 pct = g_maxCount ? ((1000*g_count)/g_maxCount + 5) / 10 : 0;
                std::cerr << g_count << "/" << g_maxCount << " (" << pct << "%)\n";

                std::cerr << "Progress:";
                for (U32 i = 0; i < g_entryCount; ++i)
                {
                    std::cerr << " |" << entryBegin[i]->permutation << "| ";
                }
                std::cerr << "\n";
            }

            auto choice = permChoices[i];
            std::swap(permChoices[i], permChoices.back());
            permChoices.pop_back();

            entries[0]->permutation[permInsertLoc] = choice;
            bool success;
            PartialFiller pf(R, S, nullptr, &success);
            if (success && arePossiblyCompatible(R, S))
            {
                if (entries[0]->permutation.size() == permInsertLoc + 1)
                {
                    cleverExhaustiveSearch(R, S, entryBegin, entries+1, entryCount-1, nullptr);
                }
                else
                {
                    if (g_currentThreadCount < g_maxActiveThreadCount)
                    {
                        ++g_currentThreadCount;
                        threads.push_back({});
                        auto& tri = threads.back();
                        tri.R = R;
                        tri.S = S;
                        tri.entries = getSortedEntries(tri.R, tri.S, false);
                        tri.permChoices = permChoices;
                        tri.thread = std::thread(cleverExhaustiveSearch<M>,
                                                 std::ref(tri.R),
                                                 std::ref(tri.S),
                                                 tri.entries.data(),
                                                 tri.entries.data() + std::distance(entryBegin, entries),
                                                 entryCount,
                                                 &tri.permChoices);
                    }
                    else
                    {
                        cleverExhaustiveSearch(R, S, entryBegin, entries, entryCount, &permChoices);
                    }
                }
            }

            permChoices.push_back(choice);
            std::swap(permChoices[i], permChoices.back());
        }

        --g_currentThreadCount;
        for (auto& t : threads)
        {
            t.thread.join();
            --g_currentThreadCount;
        }
        ++g_currentThreadCount;
        entries[0]->permutation[permInsertLoc] = Permutation::invalidEntry;
    }
}
template <U32 M, size_t Adim, size_t Bdim>
bool cleverExhaustiveSearch(DualPathIsomorphism& phi_R,
                            DualPathIsomorphism& phi_S,
                            const SearchData<M>& data,
                            Matrix<Adim, Adim, U32> matA,
                            Matrix<Bdim, Bdim, U32> matB,
                            Matrix<Adim, Bdim, U32> matR,
                            Matrix<Bdim, Adim, U32> matS)
{
    BacktrackingSearchData<M> R;
    BacktrackingSearchData<M> S;
    try
    {
        g_maxCount = 1;
        g_count = 0;

        R.phi = phi_R;
        R.ms.paths = *data.multistepPathsA;
        R.ms.free.resize(R.ms.paths.size());
        std::iota(R.ms.free.begin(), R.ms.free.end(), 0);
        R.psiMS.paths = *data.psiMultistepPathsA;
        R.psiMS.free.resize(R.psiMS.paths.size());
        std::iota(R.psiMS.free.begin(), R.psiMS.free.end(), 0);

        S.phi = phi_S;
        S.ms.paths = *data.multistepPathsB;
        S.ms.free.resize(S.ms.paths.size());
        std::iota(S.ms.free.begin(), S.ms.free.end(), 0);
        S.psiMS.paths = *data.psiMultistepPathsB;
        S.psiMS.free.resize(S.psiMS.paths.size());
        std::iota(S.psiMS.free.begin(), S.psiMS.free.end(), 0);

        R.multistepPhi.initPaths(data.multistepPathsA->data(),
                                 data.multistepPathsA->size());

        // Generate the various paths from the matrices. Note that the function
        // they all use is one taking three matrices: A,R,B. This generates
        // paths with a number of edges in A, one in R, and the remainder in S.
        // But the only requirement from that function is that a path from A can
        // continue in either A or R, that one in R starts from A and ends in S,
        // and that one starting in S ends in S.
        // By using the above facts, we can also generate paths only in A by
        // giving A thrice. When reusing the function in that way, the matrices
        // which are there only for being able to call the function but are not
        // used in any way is marked as a dummy argument after the name.
        R.multistepPhi.initPaths(getAllMultistepPaths<M+1>(matA, matR, matB /*dummy*/, M));
        R.multistepPhiInverse.initPaths(getAllMultistepPaths<M+1>(matR, matB, matB, 1));
        R.psi.initPaths(getAllMultistepPaths<2>(matR, matS, matA/*dummy*/, 1));
        R.psiInverse.initPaths(getAllMultistepPaths<M>(matA, matA/*dummy*/, matA/*dummy*/, M));

        S.multistepPhi.initPaths(getAllMultistepPaths<M+1>(matB, matS, matA/*dummy*/, M));
        S.multistepPhiInverse.initPaths(getAllMultistepPaths<M+1>(matS, matA, matA/*dummy*/, 1));
        S.psi.initPaths(getAllMultistepPaths<2>(matS, matR, matB/*dummy*/, 1));
        S.psiInverse.initPaths(getAllMultistepPaths<M>(matB, matB/*dummy*/, matB/*dummy*/, M));

        auto entries = getSortedEntries(R, S, true);

        g_entryCount = entries.size();
        g_currentThreadCount = 1;
        g_maxActiveThreadCount = std::thread::hardware_concurrency();
        std::cout << "Setting soft limit of " << g_maxActiveThreadCount << " simultaneously active threads.\n";
        std::cout << "To change this, modify g_maxActiveThreadCount in " __FILE__ ", two lines above this message." << std::endl;

        kraken::ScopeTimer $("Actual time");
        cleverExhaustiveSearch(R, S, entries.data(), entries.data(), entries.size(), nullptr);
    }
    catch (CompatibleIsomorphismsFound)
    {
        phi_R = R.phi;
        phi_S = S.phi;
        return true;
    }
    return false;
}

// Here we define A,R,B,S and m (the conditions they have to satisfy is checked
// at runtime, so if this compiles and runs, they can possibly lead to shift
// compatibility).
int main()
{
    g_useColourCodes = true;

    /*
    Matrix<1,1,U32> A{2};
    Matrix<4,4,U32> B{1,1,0,0,
                      0,0,1,1,
                      0,0,1,1,
                      1,1,0,0};
    Matrix<1,4,U32> R{2,2,2,2};
    Matrix<4,1,U32> S{1,1,1,1};

    constexpr U32 M = 3u;//*/

    //*
    Matrix<2,2,U32> A{1,3,2,1};
    Matrix<2,2,U32> B{1,6,1,1};
    Matrix<2,2,U32> R{8,3,1,16};
    Matrix<2,2,U32> S{2,3,1,1};
    constexpr U32 M = 3u;//*/

    /*
    Matrix<4,4,U32> A{0,0,1,1,
                      1,0,0,0,
                      0,1,0,0,
                      0,0,1,0};
    Matrix<4,4,U32> B = A;
    Matrix<4,4,U32> R{5,1,5,5,
                      5,5,1,0,
                      0,5,5,1,
                      1,0,5,4};
    Matrix<4,4,U32> S{1,2,2,1,
                      1,1,2,1,
                      1,1,1,1,
                      1,1,1,0};
    constexpr U32 M = 20u;//*/

    if (!areMatrixConditionsSatisfied(A, B, R, S, M))
    {
        throw std::logic_error("Matrices are invalid (they do not satisfy the "
                               "required preconditions) for the given m.");
    }

    auto phi_R = initialiseDualIsomorphism(A, R, B);
    auto phi_S = initialiseDualIsomorphism(B, S, A);

    auto multistepPathsA = getAllMultistepPaths<M+1>(A, R, B, M);
    auto multistepPathsB = getAllMultistepPaths<M+1>(B, S, A, M);

    auto psiMultistepPathsA = getAllMultistepPaths<M+2>(R, S, A, 1);
    auto psiMultistepPathsB = getAllMultistepPaths<M+2>(S, R, B, 1);

    SearchData<M> data;
    data.multistepPathsA = &multistepPathsA;
    data.multistepPathsB = &multistepPathsB;
    data.psiMultistepPathsA = &psiMultistepPathsA;
    data.psiMultistepPathsB = &psiMultistepPathsB;

    g_findAllIsomorphisms = false;

    cleverExhaustiveSearch<M>(phi_R, phi_S, data, A, B, R, S);

    std::cout << "Compatible? " << (arePhiMapsCompatible<M>(phi_R, phi_S,
                                                            multistepPathsA,
                                                            multistepPathsB,
                                                            psiMultistepPathsA,
                                                            psiMultistepPathsB) ? "Yes" : "No") << "\n";

    std::cout << "phi_R:\n" << phi_R << "\n\nphi_S:\n" << phi_S << "\n";//*/
    std::cout << "Final count: " << g_count << "\n";
}
